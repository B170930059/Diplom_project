
import React, { Component, useEffect, useState } from 'react';

import {View, ActivityIndicator} from 'react-native';

import { createDrawerNavigator } from '@react-navigation/drawer';
import {  NavigationContainer} from '@react-navigation/native';
import { 
  Provider as PaperProvider
} from 'react-native-paper';

import { DrawerContent } from './Component/store/DrawerContent';
import MainTabStore from './Component/store/MainTabStore';
import Store from './Component/store/Store';
import Order from './Component/store/Order';

import Providers from './Component/navigation/index';
import MainTabScreen from './Component/userscreens/MainTabScreen';

import NavigationContain from './Component/storelist/NavigationContainer';  //<NavigationContain/> 
import RootStack from './Component/userscreens/RootStackScreen';

const Drawer = createDrawerNavigator();

const App = () => {
    return(
    <PaperProvider>
      <NavigationContain/>
    </PaperProvider> 
     ); 
  } 

export default App;
