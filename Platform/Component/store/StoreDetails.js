import React, {Component, useState} from 'react';
import {Text, TextInput, StyleSheet ,ScrollView, View, Avatar,ImageBackground, Title,Caption,  TouchableOpacity, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FormButton from '../components/FormButton';
import auth from '@react-native-firebase/auth';
const image = require('../assets/logo.png');

const StoreDetails = ({navigation}) => {

    const exit = () =>{
        auth()
            .signOut()
            .then(() =>{
                 console.log('User signed out!')
            });  
      navigation.navigate('SplashScreen');
    }
    const savedata = () => {
    database()
      .ref(`storelist/${storename}`)
      .set({
        storename: {storename},
        storephone: {storephone},
        email: {email},
        address: {address},
      })
      .then(() => {
        navigation.navigate('MainStore');
        console.log("Inserted");
      })
    }
    return(
        <ScrollView style = {styles.container}>
           <View style={{padding:10, margin:5, flexDirection:'row', width:'100%'}}>
                <ImageBackground
                     source={image}
                    style={{height: 50, width: 50}}
                    imageStyle={{borderRadius: 15}}
                    />
                <View style={{flexDirection:'column', paddingLeft: 10, width:'72%'}}>    
                    <Text style={styles.textstyle}>Emart</Text>
                    <Text style={styles.textstyle}>75758585</Text>
                </View>
                <Icon
                name="exit-to-app"
                size={26}
                onPress={()=>exit()}
                />
          </View>      
          <View style={{padding:10}}>
            <Text style={{fontSize:16, color:'#4F4F4F'}}>Дэлгүүрийн нэр</Text>   
            <TextInput 
              value=""
              onChangeText=""
              placeholder="Emart"
              keyboardType="web-search"
              style={{
                  backgroundColor:'white',
                  marginTop:10,
                  borderRadius:10,
                  height:40,
                  color:'red'
              }}
            />
            <Text style={{fontSize:16, color:'#4F4F4F', paddingTop:8}}>Утас</Text>   
            <TextInput 
              value=""
              onChangeText=""
              placeholder="85857575"
              keyboardType="web-search"
              style={{
                  backgroundColor:'white',
                  marginTop:10,
                  borderRadius:10,
                  height:40,
                  color:'red'
              }}
            />
            <Text style={{fontSize:16, color:'#4F4F4F', paddingTop:8}}>И-мэйл хаяг</Text>   
            <TextInput 
              value=""
              onChangeText=""
              placeholder="имэйл"
              keyboardType="web-search"
              style={{
                  backgroundColor:'white',
                  marginTop:10,
                  borderRadius:10,
                  height:40,
                  color:'red'
              }}
            />
            <Text style={{fontSize:16, color:'#4F4F4F', paddingTop:8}}>Дэлгүүрийн хаяг</Text>   
            <TextInput 
              value=""
              onChangeText=""
              placeholder="хаяг"
              keyboardType="web-search"
              style={{
                  backgroundColor:'white',
                  marginTop:10,
                  borderRadius:10,
                  height:40,
                  color:'red'
              }}
            />
            <Text style={{fontSize:16, color:'#4F4F4F', paddingTop:8}}>Төлбөрийн нөхцөл</Text>   
            <TextInput 
              value=""
              onChangeText=""
              placeholder="төлбөр"
              keyboardType="web-search"
              style={{
                  backgroundColor:'white',
                  marginTop:10,
                  borderRadius:10,
                  height:70,
                  color:'red'
              }}
            />
            <Text style={{fontSize:16, color:'#4F4F4F', paddingTop:8}}>Хүргэлтийн нөхцөл</Text>   
            <TextInput 
              value=""
              onChangeText=""
              placeholder="хүргэлт"
              keyboardType="web-search"
              style={{
                  backgroundColor:'white',
                  marginTop:10,
                  borderRadius:10,
                  height:70,
                  color:'red'
              }}
            />
            <FormButton 
            style={{paddingTop:10}}
              buttonTitle = "Хадгалах"
              onPress={() => {}}
            />
           
          </View>
          
       </ScrollView>
    )
}
 const styles = StyleSheet.create({
     container: {
         flex: 1,
         margin:8,
         backgroundColor:'#E5E4E4',
         borderRadius: 10,
     },
     textstyle: {
         fontSize: 18,
         color: 'black',
     },
     
      
 })

 export default StoreDetails;