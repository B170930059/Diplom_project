import React, {useEffect, useState} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button,
    TouchableOpacity,
    ImageBackground,
} from 'react-native';
import {Checkbox} from 'react-native-paper'; 
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'; 
import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import {AuthContext} from '../navigation/AuthProvider';
import database from '@react-native-firebase/database';
const AddProduct = ({navigation})=>{
    const [checked, setChecked] = useState(false);
    const [category, setCategory] = useState([]);
    //const [name, setName] = useState([]);

    // const display = async () => {
    //     const userRef = database().ref('/category');
    //     const listener = userRef.on('value', snapshot => {
    //        setCategory([]);
    //        snapshot.forEach(function(childSnapshot){
    //            setCategory(category => [...category, childSnapshot.val()]);
    //        });
    //    }); 
    //    return () => {
    //        userRef.off('value', listener);
    //    }
    // }
    useEffect(()=>{
        const userRef =  database().ref('/category');
        const listener = userRef.on('value', (snapshot)=> {
      
      let temp = [];
      snapshot.forEach((doc) => {
        let data = doc.val();
        temp.push(data);
      }); 
      setCategory(temp);
     // console.log(store);
    });
    return () =>{
      userRef.off('value', listener);
    }
     });
    return(
   <View style={styles.container}>
       <Text style={styles.textt } onPress={()=>alert('hi')} >Ангилалаа сонгоно уу:</Text>
          {
              category.map((data,  i) => {
              return(
                
                  <View  style={{padding:10,margin:3, flexDirection:'row'}} key={i}>
                   <Text style={styles.text}  onPress={()=>navigation.navigate('AddProductDetails', {category: data})}>{data}</Text>
                      {/* <Checkbox status={checked ? 'checked' : 'unchecked'}
                        onPress={() => {
                        setChecked(!checked);
                      }}/> */}
                </View>
               
               );
              })
          } 
         
       {/* <FormButton
       style={{flex:2, justifyContent:'flex-end', }}
        buttonTitle="Үргэлжлүүлэх"
        onPress={()=> navigation.navigate('AddProductDetails', {category: category})}
      /> */}
   </View>
    );
}
export default AddProduct;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding:10,
        margin: 10,
    },
    textt:{
        fontSize: 20,
        color:'black',
        paddingLeft:5,
        fontWeight: 'bold',
        
    },
    text:{
        fontSize: 16,
        color:'black',
        paddingLeft:5,
        width:'90%'

    },
    signIn: {
    width: 140,
    height: 50,
    color:'#000000',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    flexDirection:'row',
    backgroundColor:'#08AAC1',
    
    
},

})