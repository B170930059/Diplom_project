import React, {useState} from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    TextInput,
    Platform,
    StyleSheet ,
    StatusBar,
    Alert,
    ToastAndroid,
    FlatList,
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import { colors } from 'react-native-elements';
import { useTheme } from '@react-navigation/native';
import '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-async-storage/async-storage';

import database from '@react-native-firebase/database';
import { cos } from 'react-native-reanimated';
 const CreateStore = ({navigation, route}) =>{
  //const [user, setUser] = useState(route.params);
  const user = route.params;
  const [storename, setStoreName] = useState();
  const [storephone, setPhone] = useState();
  const [email, setEmail] = useState();
  const [address, setAddress] = useState();
 
  const createstore = () => {
    database()
      .ref(`storelist/${storename}`)
      .set({
        storename: storename,
        storephone: storephone,
        email: email,
        address: address,
        user: user
      })
      .then(() => {
        savestoredetail();
       navigation.navigate('MainStore',{
          storename: storename,
        });
        console.log("Inserted");
      })
  }
  const savestoredetail = () =>{
    try{
      const value = {
         storename: storename,
         storephone: storephone, 
      }
        AsyncStorage.setItem('storename', JSON.stringify(value))
    }
    catch(er){
      console.log(er);
    }
  }
   return(
       <View style={styles.container}>
             <View style={{padding:10, alignItems:'center'}}>
                <Text style={{fontSize: 20, }} onPress={()=> {}}>Дэлгүүрийн мэдээлэл оруулна уу</Text>
             </View>
         <View style={styles.action}>
             <Icon name="store" color={colors.text} size={25}/>
             <TextInput 
               placeholder="Дэлгүүрийн нэр"
               placeholderTextColor="#666666"
               autoCorrect={false}
               style={styles.textInput}
               value={storename}
               onChangeText={(storename)=> setStoreName(storename)}
               />
         </View>  
         <View style={styles.action}>
             <Feather name="phone" color={colors.text} size={20}/>
             <TextInput 
               placeholder="Дэлгүүрийн утас"
               placeholderTextColor="#666666"
               autoCorrect={false}
               style={styles.textInput}
               value={storephone}
               onChangeText={(storephone)=> setPhone(storephone)}
               />
         </View>  
         <View style={styles.action}>
             <FontAwesome name="envelope-o" color={colors.text} size={20}/>
             <TextInput 
               placeholder="И-мэйл хаяг"
               placeholderTextColor="#666666"
               autoCorrect={false}
               style={styles.textInput}
               value={email}
               onChangeText={(email)=> setEmail(email)}
               />
         </View>  
         <View style={styles.action}>
             <Icon name="map-marker-radius" color={colors.text} size={25}/>
             <TextInput 
               placeholder="Дэлгүүрийн хаяг"
               placeholderTextColor="#666666"
               autoCorrect={false}
               style={styles.textInput}
               value={address}
               onChangeText={(address)=> setAddress(address)}
               />
         </View>  
          <TouchableOpacity style={styles.commandButton} onPress={() => createstore()}>
               <Text style={styles.panelButtonTitle}>Дэлгүүр үүсгэх</Text>
            </TouchableOpacity>         
      </View>
   );
}
export default CreateStore;

 const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 10
    },
    commandButton: {
      padding: 15,
      borderRadius: 10,
      backgroundColor: '#D0C402',
      alignItems: 'center',
      marginTop: 10,
    },
    panel: {
      padding: 20,
      backgroundColor: '#FFFFFF',
      paddingTop: 20,
      // borderTopLeftRadius: 20,
      // borderTopRightRadius: 20,
      // shadowColor: '#000000',
      // shadowOffset: {width: 0, height: 0},
      // shadowRadius: 5,
      // shadowOpacity: 0.4,
    },
    header: {
      backgroundColor: '#FFFFFF',
      shadowColor: '#333333',
      shadowOffset: {width: -1, height: -3},
      shadowRadius: 2,
      shadowOpacity: 0.4,
      // elevation: 5,
      paddingTop: 20,
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20,
    },
    panelHeader: {
      alignItems: 'center',
    },
    panelHandle: {
      width: 40,
      height: 8,
      borderRadius: 4,
      backgroundColor: '#00000040',
      marginBottom: 10,
    },
    panelTitle: {
      fontSize: 27,
      height: 35,
    },
    panelSubtitle: {
      fontSize: 14,
      color: 'gray',
      height: 30,
      marginBottom: 10,
    },
    panelButton: {
      padding: 13,
      borderRadius: 10,
      backgroundColor: '#FF6347',
      alignItems: 'center',
      marginVertical: 7,
    },
    panelButtonTitle: {
      fontSize: 17,
      fontWeight: 'bold',
      color: 'white',
    },
    action: {
      flexDirection: 'row',
      marginTop: 10,
      marginBottom: 10,
      borderBottomWidth: 1,
      borderBottomColor: '#f2f2f2',
      padding: 10
    },
    actionError: {
      flexDirection: 'row',
      marginTop: 10,
      borderBottomWidth: 1,
      borderBottomColor: '#FF0000',
      paddingBottom: 5,
    },
    textInput: {
      flex: 1,
      marginTop: Platform.OS === 'ios' ? 0 : -12,
      paddingLeft: 10,
      color: '#05375a',
    },
  });