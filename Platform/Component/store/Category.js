import React, {useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button,
    TouchableOpacity,
    ImageBackground,
    Alert
} from 'react-native';
//import { Icon } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';
import Icon from 'react-native-vector-icons/MaterialIcons'; 
import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createIconSetFromFontello } from 'react-native-vector-icons';
const Category = ({navigation})=>{
    const [category, setCategory] = useState([]);

    useEffect(()=>{
        const userRef =  database().ref('/category');
        const listener = userRef.on('value', (snapshot)=> {
      
        let temp = [];
        snapshot.forEach((doc) => {
            let data = doc.val();
            temp.push(data);
        }); 
        setCategory(temp);
        // console.log(store);
        });
        return () =>{
        userRef.off('value', listener);
        }
    });
    return(
   <View style={styles.container}>
       {/* <View style={{padding:10,margin:3}}>  
           <TouchableOpacity style={{flexDirection:'row', width:'100%'}}>
             <Text style={styles.text} onPress={()=> getData()}>Хувцас</Text>
             <Icon name="keyboard-arrow-down" size={26}/>
           </TouchableOpacity>
       </View> */}
       {
            category.map((item, index) =>{
            return(
                <View style={{flexDirection:'row'}} key={index}>
                    <Text style={{paddingTop:10, paddingBottom:10, color:'black', fontSize: 16, paddingLeft:10, width:'80%'}} >{item}</Text>
                    <Icon name="arrow-drop-down" size={25} style={{color:'#646464'}}/>
                </View>
            );
          })
       }
       <View style={{flex:2, justifyContent:'flex-end', alignItems:'flex-end'}}>
            <TouchableOpacity style={styles.signIn} onPress={()=> navigation.navigate('AddCategory')}>
                <Icon name="create-new-folder" size={21} style={{color:'#646464'}}/>
                <Text style={{fontSize:15, paddingLeft:5,  color:'#646464',}}>Ангилал нэмэх</Text>
            </TouchableOpacity>
         </View>
   </View>
    );
}
export default Category;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding:10,
        margin: 10,
    },
    textt:{
        fontSize: 20,
        color:'black',
        paddingLeft:5,
        fontWeight: 'bold',
        
    },
    text:{
        fontSize: 16,
        color:'black',
       // paddingLeft:5,
        width:'90%'

    },
    signIn: {
    width: 150,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    flexDirection:'row',
    backgroundColor:'#D0C402',
    
    
},

})