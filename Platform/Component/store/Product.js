import { Navigation } from '@material-ui/icons';
import React, {Components} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button,
    TouchableOpacity,
} from 'react-native';
//import { Icon } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';
import Icon from 'react-native-vector-icons/MaterialIcons'; 

const Product = ({navigation})=>{
    return(
   <View style={styles.container}>
         <View>
            <TouchableOpacity style={{flexDirection:'row', paddingBottom:10}} onPress={()=>navigation.navigate('AllProduct')}>
               <Icon name="view-stream" size={23} style={{color:'black'}}/> 
               <Text style={styles.text}>Бүх бараа</Text>
             </TouchableOpacity>
             <TouchableOpacity style={{flexDirection:'row'}} onPress={()=> navigation.navigate('Category')}>
               <Icon name="category" size={23} style={{color:'black'}}/> 
               <Text style={styles.text}>Бүх ангилал</Text>
             </TouchableOpacity>
         </View>
         <View style={{flex:2, justifyContent:'flex-end', alignItems:'flex-end'}}>
            <TouchableOpacity style={styles.signIn} onPress={()=> navigation.navigate('AddProduct')}>
                <Icon name="add-circle" size={21} style={{color:'#646464'}}/>
                <Text style={{fontSize:15,color:'#646464', paddingLeft:5}}>Бараа нэмэх</Text>
            </TouchableOpacity>
         </View>
    </View>
    );
}

export default Product;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding:10,
        margin: 10,
    },
    text:{
        fontSize: 20,
        color:'black',
        paddingLeft:5

    },
    signIn: {
    width: 140,
    height: 50,
    color:'#000000',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    flexDirection:'row',
    backgroundColor:'#D0C402', //#08AAC1
    
    
},

})