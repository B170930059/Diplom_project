import React, {useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button,
    TouchableOpacity,
    Image,

} from 'react-native';
//import { Icon } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';
import Icon from 'react-native-vector-icons/MaterialIcons';
import database from '@react-native-firebase/database'; 
const image = require('../assets/zurag.png');
const AllProduct = ({navigation})=>{
    const [product, setProduct] = useState([]);
    useEffect(()=>{
        const userRef =  database().ref('/product');
        const listener = userRef.on('value', (snapshot)=> {
      
      let temp = [];
      snapshot.forEach((doc) => {
        let data = doc.val().productname;
        temp.push(data);
      }); 
      setProduct(temp);
     // console.log(store);
    });
    return () =>{
      userRef.off('value', listener);
    }
     });

    return(
   <View style={styles.container}>
       {
            product.map((data, i) => {
                return(
                    <TouchableOpacity onPress={()=>navigation.navigate('EditProduct', {product: data})} style={{backgroundColor:'white',padding:8,marginBottom:10, borderRadius:8}}>  
                    <View style={{flexDirection:'row'}}>
                      <Image source={image} size={20} style={{width:50, height:50}}/>
                       <View style={{}}>
                           <Text style={styles.text}> {data}</Text>
                           <Text style={styles.text}> {}</Text>
                       </View>
                   </View>
                 </TouchableOpacity>  
                )
            })
        }
    </View>
    );
}
export default AllProduct;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding:10,
        margin: 10,
    },
    text:{
        fontSize: 20,
        color:'black',
        paddingLeft:5

    },
    signIn: {
    width: 140,
    height: 50,
    color:'#000000',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    flexDirection:'row',
    backgroundColor:'#08AAC1',
    
    
},

})