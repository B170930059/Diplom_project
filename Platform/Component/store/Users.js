import React, { useEffect, useState } from 'react';
import { View, Text, Button, StyleSheet, Touchable, Alert, Picker } from 'react-native';
import { fonts } from 'react-native-elements/dist/config';
import { TouchableOpacity } from 'react-native-gesture-handler';
import  {Avatar} from 'react-native-paper';
//import {Dropdown} from 'react-native-material-dropdown';
import ModalDropdown from 'react-native-modal-dropdown';
const image = require('../assets/man.png');
import AsyncStorage from '@react-native-async-storage/async-storage';
const Users = ({navigation}) => {
  const [user, setUser] = useState('');
  let [order, setOrder] = useState(0);
 const display =()=>{
   try{
      AsyncStorage.getItem('user').then(data=>{
        setUser(JSON.parse(data));
        setOrder(1);
      })
   }
   catch(er){
     console.log(er);
   }
 }
  return (
      <View style={styles.container}>  
         <TouchableOpacity onPress={()=> display()}>
         <View style={{flexDirection:'row', paddingTop: 10}}>
            <Avatar.Image 
                source={image}

                size={70}
            />
            <View style={{flexDirection:'column'}}>
                <Text style={styles.text}>
                    {user.name}
                </Text>
                <Text style={{fontSize:16, color: '#493F50', paddingLeft:13}}>
                    {user.phone}
                </Text>     
                <Text style={{fontSize:16, color: '#493F50', paddingLeft:13}}>
                    Нийт захиалга: {order}
                </Text>  
            </View>    
          </View>
         </TouchableOpacity>       
      </View>
    );
};

export default Users;

const styles = StyleSheet.create({
  container: {
    padding: 17,
    flex: 1, 
    alignItems: 'flex-start', 
    justifyContent: 'flex-start'
  },
  header:{
    flexDirection: 'row',
    paddingBottom:10,
    
  },
  text:{
    fontSize: 18,
    paddingLeft: 13,
    flexDirection:'column',
    color: '#493F50',
  }
});