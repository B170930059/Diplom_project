import React, {useState} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button,
    TouchableOpacity,
    ImageBackground,
    Alert
} from 'react-native';
//import { Icon } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'; 
import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import AddProduct from './AddProduct';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';
const images  = require('../assets/product.png');
const AddProductDetails = ({navigation, route})=>{
    const [pname, setName] = useState();
    const [pcode, setCode] = useState();
    const [price, setPrice] = useState();
    const [quant, setQuant] = useState();
    const [comment, setComment] = useState();
    const {category} = route.params;
    const value = {
      category: category,
      productname: pname,
      productcode: pcode,
      price: price,
      quantity: quant,
      description: comment
    }
    const addProduct = () => {
      database()
        .ref(`product/${pcode}`)
        .set(value)
        .then(()=> Alert.alert("Бараа","Амжилттай нэмэгдлээ"));
    }
    return(
   <View style={styles.container}>
       <TouchableOpacity onPress={() => {}} >
                <View
                   style={{
                     height: 100,
                     width: 100,
                     borderRadius: 15,
                     justifyContent:'center',
                     alignItems: 'center',
                   }}
                 >
                   <ImageBackground
                     source={images}
                    style={{height: 70, width: 70}}
                    imageStyle={{borderRadius: 15}}
                    >
                      <View style={{
                          flex: 1,
                          justifyContent: 'center',
                          alignItems: 'center',
                      }}>
                         <Icon name="camera" size={35} color="#fff" style={{
                            opacity: 0.7,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderWidth: 1,
                            borderColor: '#fff',
                            borderRadius: 10,
                         }}/>
                      </View>
                    </ImageBackground>
                 </View>
             </TouchableOpacity>
       
       <FormInput
            labelValue={pname}
            onChangeText={(pname) => setName(pname)}
            placeholderText="Барааны нэр"
            keyboardType="text"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormInput
            labelValue={pcode}
            onChangeText={(pcode) => setCode(pcode)}
            placeholderText="Барааны код"
            keyboardType="text"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormInput
            labelValue={price}
            onChangeText={(price) => setPrice(price)}
            placeholderText="Үнэ"
            keyboardType="numeric"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormInput
            labelValue={quant}
            onChangeText={(quant) => setQuant(quant)}
            placeholderText="Тоо ширхэг"
            keyboardType="numeric"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormInput
            height={80}
            labelValue={comment}
            onChangeText={(comment) => setComment(comment)}
            placeholderText="Тайлбар"
            keyboardType="text"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormButton
        buttonTitle="Хадгалах"
        onPress={()=> addProduct()}
      />
   </View>
    );
}
export default AddProductDetails;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding:10,
        margin: 10,
    },
    textt:{
        fontSize: 20,
        color:'black',
        paddingLeft:5,
        fontWeight: 'bold',
        
    },
    text:{
        fontSize: 16,
        color:'black',
        paddingLeft:5,
        width:'90%'

    },
    signIn: {
    width: 140,
    height: 50,
    color:'#000000',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    flexDirection:'row',
    backgroundColor:'#08AAC1',
    
    
},

})