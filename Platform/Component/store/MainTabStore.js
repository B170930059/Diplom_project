import React, { useState, useEffect } from 'react';
import {Text} from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Settings from './Settings';
import Category from './Category'
import Product from './Product';
import Users from './Users';
import Order from './Order';
import Store from './Store';
import AddProduct from './AddProduct';
import { useTheme } from '@react-navigation/native';
import AllProduct from './AllProduct';
import AddProductDetails from './AddProductDetails';
import  EditProduct from './EditProduct';
import AddCategory from './AddCategory';
import OrderDetail from './OrderDetail';
import StoreDetail from './StoreDetails';

import CreateStore from '../store/CreateStore';
import { DrawerActions } from '@react-navigation/native';
import database from '@react-native-firebase/database';
const StoreStack = createStackNavigator();
const ProductStack = createStackNavigator();
const UsersStack = createStackNavigator();
const OrderStack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const MainTabStore = ({route}) => { 
 return(
    <Tab.Navigator
      initialRouteName="Store"
      activeColor="#fff"
      inactiveColor="#2E2E2E"
      barStyle={{backgroundColor:'#D0C402'}}
    >
      <Tab.Screen
        name="StoreStack"
        component={StoreStackScreen}
        options={{
          tabBarLabel: 'Нүүр',
          //tabBarLabel: <Text>Emart</Text>,
         // tabBarColor: '#7207B3',
          tabBarIcon: ({ color }) => (
            <Icon name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Product"
        component={ProductStackScreen}
        options={{
          tabBarLabel: 'Бараа',
         // tabBarColor: '#7207B3',
          tabBarIcon: ({ color }) => (
            <Icon name="local-offer" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Order"
        component={OrderStackScreen}
        options={{
          tabBarLabel: 'Захиалга',
        //  tabBarColor: '#7207B3',
          tabBarIcon: ({ color }) => (
            <Icon name="notifications-none" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Users"
        component={UsersStackScreen}
        options={{
          tabBarLabel: 'Хэрэглэгч',
       //   tabBarColor: '#7207B3',
          tabBarIcon: ({ color }) => (
            <Icon name="supervised-user-circle" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default MainTabStore;
 

const StoreStackScreen = ({navigation, route}) => {

 const [sname, setName] = useState('');
 useEffect(()=>{
   try{
    AsyncStorage.getItem('storename').then(data =>{
       const val = JSON.parse(data);
       setName(val.storename);
    })
   }
   catch(er){
     console.log(er);
   }
 })
  return(
      <StoreStack.Navigator 
        screenOptions={{
          headerStyle: {
          backgroundColor: '#D0C402',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          alignSelf: 'flex-start'
          }
    }}>
      
      <StoreStack.Screen 
        name="Store" 
        component={Store} 
        options={({route}) => ({
        title: <Text>{sname}</Text>,
          //title: route.params.storename,
        headerLeft: ()=> (
          <Icon.Button 
             name='menu'
             size={25}
             backgroundColor='#D0C402'
             color='#fff'
             onPress={()=> navigation.navigate('StoreDetail')}
             />
        ),
        headerTitleAlign:'center',
        })
       } />
        <StoreStack.Screen 
          name="StoreDetail"
          component={StoreDetail}
          options={{
            title:'Тохиргоо',
            headerTitleAlign:'center'
          }}
        />
   </StoreStack.Navigator>
  )
};
  
const OrderStackScreen = ({navigation}) => {
  return(
   <OrderStack.Navigator
   screenOptions={{
    headerStyle: {
      backgroundColor: '#D0C402',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
    fontWeight: 'bold',
    alignSelf: 'center',
    }
}}>
    <OrderStack.Screen
      name="Order"
      component={Order}
      options={{
         title: 'Захиалга',
         headerLeft: () => null
      }}
    />
    <OrderStack.Screen
      name="OrderDetail"
      component={OrderDetail}
      options={{
         title: 'Захиалгын дэлгэрэнгүй',
         headerTitleAlign:'center'
      }}
      
    />
   </OrderStack.Navigator>
  );
}
const ProductStackScreen = ({navigation}) => {
  return(
  <ProductStack.Navigator 
        screenOptions={{
          headerStyle: {
          backgroundColor: '#D0C402',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
          //fontWeight: 'bold',
          shadowColor: '#fff',
          }
      }}>
          <ProductStack.Screen 
          name="Product" 
          component={Product} 
          options={{ title: 'Барааны мэдээлэл',
          headerLeft: ()=>null,
          headerTitleAlign:'center',
          }} />
          <ProductStack.Screen
            name="AddProduct"
            component={AddProduct}
            options={{title: 'Бараа нэмэх',
            headerTitleAlign:'center',
          }}
          />
          <ProductStack.Screen
            name="AddProductDetails"
            component={AddProductDetails}
            options={{title: 'Бараа нэмэх',
            headerTitleAlign:'center', 
          }}
          />
          <ProductStack.Screen
            name="Category"
            component={Category}
            options={{title: 'Ангилал',
            headerTitleAlign:'center', 
          }}
          />
          <ProductStack.Screen
            name="AllProduct"
            component={AllProduct}
            options={{title: 'Бүх бараа',
            headerTitleAlign:'center', 
          }}
          />
          <ProductStack.Screen
          name="EditProduct"
          component={EditProduct}
          options={{title: 'Барааны нэр',
          headerTitleAlign:'center', 
        }}
        />
         <ProductStack.Screen
          name="AddCategory"
          component={AddCategory}
          options={{title: 'Ангилал нэмэх',
          headerTitleAlign:'center', 
        }}
        />
          
  </ProductStack.Navigator>
  );
}
  const UsersStackScreen = ({navigation}) => {
  return(
    <UsersStack.Navigator 
          screenOptions={{
            headerStyle: {
            backgroundColor: '#D0C402',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
            fontWeight: 'bold',
            alignSelf: 'center',
            shadowColor: '#fff'
            }
        }}>
            <UsersStack.Screen 
            name="Users" 
            component={Users} 
            options={{ title: 'Хэрэглэгчид',
            headerLeft:()=>null,
            }} />
    </UsersStack.Navigator>
    );
  }