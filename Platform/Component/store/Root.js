import React from 'react';


import Category from './Category';
import AllProduct from './AllProduct';
import AddProduct from './AddProduct';
import AddProductDetails from './AddProductDetails';
import Product from './Product';
import { createStackNavigator } from '@react-navigation/stack';

const RootStack  = createStackNavigator();

const RootStackScreen = ({navigation}) => (
  <RootStack.Navigator headerMode='Product'
  screenOptions={{
    headerStyle: {
    backgroundColor: '#7207B3',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
    fontWeight: 'bold',
    shadowColor: '#fff',
    }
}}
   >
        <RootStack.Screen
            name="AddProduct"
            component={AddProduct}
            options={{title: 'Бараа нэмэх',
            headerTitleAlign:'center',
          }}
          />
          <RootStack.Screen
            name="AddProductDetails"
            component={AddProductDetails}
            options={{title: 'Бараа нэмэх',
            headerTitleAlign:'center', 
          }}
          />
          <RootStack.Screen
            name="Category"
            component={Category}
            options={{title: 'Ангилал',
            headerTitleAlign:'center', 
          }}
          />
          <RootStack.Screen
            name="AllProduct"
            component={AllProduct}
            options={{title: 'Бүх бараа',
            headerTitleAlign:'center', 
          }}
          />
  </RootStack.Navigator>
);

export default RootStackScreen;