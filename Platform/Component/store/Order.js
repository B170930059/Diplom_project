import React, { useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet, Touchable, Alert, Picker } from 'react-native';
import { fonts } from 'react-native-elements/dist/config';
import { TouchableOpacity } from 'react-native-gesture-handler';
import  {Avatar} from 'react-native-paper';
import ModalDropdown from 'react-native-modal-dropdown';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';

const image = require('../assets/order.png');

const Order = ({navigation}) => {
 const [ordercode, setOrderCode] = useState([]);
 const [orderdate, setDate] = useState([]);

 useEffect(()=>{
    try{
      const ref = database().ref('/order');
      const listener = ref.on('value', (snapshot)=> {
      
        let temp = [];
        snapshot.forEach((doc) => {
          let data = doc.val();
          temp.push(data);
        }); 
        setOrderCode(temp);
      });
      return () =>{
        ref.off('value', listener);
      }
    }
    catch(err){
      console.log(err);
    }
 });
 
  return (
      <View style={styles.container}>
        <View style={styles.header}>
           <Text style={{fontSize: 18, color: '#493F50'}} onPress={()=> console.log(ordercode[3])}>Нийт захиалга:</Text>
           <Text style={{fontSize: 18, color: '#493F50', paddingLeft:5}}>{ordercode.length}</Text>  
        </View> 
        {
           ordercode.map((item, index)=>{
             return(
              <TouchableOpacity onPress={()=>navigation.navigate('OrderDetail')} >
              <View style={{flexDirection:'row', paddingTop: 10}}>
                 <Avatar.Image 
                     source={image}
                     size={50}
                 />
                 <View style={{flexDirection:'column'}}>
                     <Text style={styles.text} >
                         {item.id}
                     </Text>
                     <Text style={styles.text}>
                         {item.date}
                     </Text>     
                 </View>    
               </View>
              </TouchableOpacity>    
             );
           })
        }
        
         
      </View>
    );
};

export default Order;

const styles = StyleSheet.create({
  container: {
    padding: 17,
    flex: 1, 
    alignItems: 'flex-start', 
    justifyContent: 'flex-start'
  },
  header:{
    flexDirection: 'row',
    paddingBottom:10,
    
  },
  text:{
    fontSize: 18,
    paddingLeft: 13,
    flexDirection:'column',
    color: '#493F50',
  }
});