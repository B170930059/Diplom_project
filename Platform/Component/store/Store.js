import React, {useContext, useEffect, useState} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button,
    Image
} from 'react-native';
import { AuthContext } from '../navigation/AuthProvider';
import {Avatar, Title, Caption} from 'react-native-paper';
import FormButton from '../components/FormButton';
import { Item } from 'react-native-paper/lib/typescript/components/List/List';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';
const image = require('../assets/logo.png');
const imag = require('../assets/promax.png');
const imagu = require('../assets/airpod.png');
const Store = ()=> {
  const [order_, setOrder] = useState([]);
  const [allproduct, setAllproduct] = useState([]);
  const [storedetail, setDetail] = useState();
  
  const [sname, setName] = useState('');
  const [sphone, setPhone] = useState('');
  useEffect(() => {
    try{
      const ref = database().ref('/product');
      const listener = ref.on('value', (snapshot) => {
      let temp = [];
      snapshot.forEach((doc) => {
        let data = doc.val();
        temp.push(data);
      });
      setAllproduct(temp);
      order();
      displayname();
    });
  return()=> {
    ref.off('value', listener);
  }
    }
    catch(er){
      console.log(er);
    }
  });
  const displayname =()=>{
    try{
     AsyncStorage.getItem('storename').then(data =>{
        const val = JSON.parse(data);
        setName(val.storename);
        setPhone(val.storephone);
     })
    }
    catch(er){
      console.log(er);
    }
  }
 
 
  const order = () =>{
    try{   
    const ref = database().ref('/order');
    const listener = ref.on('value', (snapshot) => {
      let temp = [];
      snapshot.forEach((doc) => {
        let data = doc.val().id;
        temp.push(data);
      });
      setOrder(temp);
    });
    return()=> {
      ref.off('value', listener);
    }
    }
    catch(er){
      console.log(er);
    }
  }
  const savedata = () =>{
    try{
      AsyncStorage.getItem('storename').then(data=>{
        setDetail(JSON.parse(data));
       });
   }
   catch(er){
     console.log(er);
   }
  }
  // {storedetail.storename}
  // {storedetail.storephone}
   return(
    
    <View style={styles.container}>
      <View style={{flexDirection:'row',marginTop: 15}}>
    <Avatar.Image 
          source={image}
          size={50}
          />
      <View style={{marginLeft:15, flexDirection:'column'}}>
        <Title style={[styles.title], {color:'#4F4B4A'}} onPress={()=>savedata()}>{sname}</Title>
        <Caption style={[styles.caption],{fontSize:18}}>{sphone}</Caption>
      </View>
     </View> 
     <View style={{paddingTop: 20}}>
       <Text style={{fontSize: 20, color:'#221D6A'}}  >Тайлан</Text>
       <View style={{flexDirection:'row', paddingTop: 20}}>
           <View>
             <Text style={[styles.texts]} >{allproduct.length}</Text>
             <Text style={[styles.text]}>Нийт бараа</Text>
           </View>
           <View>
             <Text style={[styles.texts]} >{order_.length}</Text>
             <Text style={[styles.text]}>Нийт захиалга</Text>
           </View>
           <View>
             <Text style={[styles.texts]} >{order_.length}</Text>
             <Text style={[styles.text]}>Нийт хэрэглэгчид</Text>
           </View>
       </View>   
    </View>   

    <View style={{paddingTop: 20, width: '100%'}}>
    <Text style={{fontSize: 20, color:'#221D6A', paddingBottom: 20}}>Сүүлд нэмэгдсэн бараанууд</Text>
         <View> 
          <Image
             source={imagu}
             resizeMode="cover"
             style={{
               width: '100%',
               height: 200,
               borderRadius: 15
             }}
          />

          <View style={{
            position: 'absolute',
            bottom:0,
            height: 35,
            width: '35%',
            backgroundColor: 'white',
            borderTopRightRadius: 15,
            borderBottomLeftRadius: 15,
            alignContent:'center',
            justifyContent:'center',

          }}>
             <Text style={{}}>Airpod pro 4</Text>             
          </View>
          </View>
          <Text style={styles.ptitle}>Тайлбар</Text>
          <Text style={styles.ptitle}>Үнэ:</Text>
    </View>           
  </View>
  );
}

export default Store;

const styles = StyleSheet.create({
  container: {
    //backgroundColor: '#f9fafd',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingLeft: 15,
    paddingRight: 15,
  },
  texts:{
    fontSize: 20,
    color: '#0B038D', 
    fontWeight:'bold', 
    minWidth: '32%',
    textAlign:'center'
  },
  text:{
    fontSize:12,
    color:'#5D5B5B',
    textAlign:'center',
  },
  ptitle:{
    fontSize: 18,
    fontWeight: 'bold',

  }

});