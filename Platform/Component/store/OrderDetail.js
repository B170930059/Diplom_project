import React, {useState, useEffect} from 'react';
import { View, Text, Button, StyleSheet, Touchable, ScrollView ,Alert, Image } from 'react-native';
import { fonts } from 'react-native-elements/dist/config';
import { TouchableOpacity } from 'react-native-gesture-handler';
import  {Avatar} from 'react-native-paper';
//import {Dropdown} from 'react-native-material-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ModalDropdown from 'react-native-modal-dropdown';
import AsyncStorage from '@react-native-async-storage/async-storage';
import database from '@react-native-firebase/database';

const image = require('../assets/airpod.png');

const OrderDetail = ({navigation}) => {
  const [ordername, setOrder] = useState();
  const [phone, setPhone] = useState();
  const [tprice, setTPrice] = useState();
  const [product, setProduct] = useState([]);
    useEffect(()=>{
       try{ 
          const ref = database().ref('/order');
          const listener = ref.on('value', (data) =>{
            let val1, val2, val3;
            data.forEach(doc=>{
              val1 = doc.val().name,
              val2 = doc.val().phone
              val3 = doc.val().total_price
            });
             setOrder(val1);
             setPhone(val2);
             setTPrice(val3);
          })
          getProduct();
          return()=>{
            ref.off('value', listener);
          }
       }
       catch(er){
         console.log(er);
       }
  });

  const getProduct = () =>{
    try{
      AsyncStorage.getItem('order').then((item)=>{
        let temp = [];
        if(item !== null){
          let val = JSON.parse(item);
          setProduct(val);
        }
        });
    } 
    catch(err){
      console.log(err);
    }
  }

  return (
      <View style={styles.container}>
        <View style={styles.header}>
           <Text style={{fontSize: 18, color: '#493F50'}} onPress={()=> console.log(product)}>Захиалагчийн мэдээлэл</Text>
          </View>    

        <View style={{padding:10, paddingLeft:20}}>
           <View style={{flexDirection:'row', paddingBottom:10}}> 
            <Text style={styles.text}>
                Төлөв
            </Text>
            <ModalDropdown style={{}}
                 options={['Төлбөр төлсөн', 'Хүлээгдэж буй']}
                 defaultValue={'Төлөв сонгоно уу'}
                    textStyle={{fontSize:15, color:'#545453'}}
                    dropdownStyle={{height:60, width:130}}
                 />
            <Icon name="arrow-drop-down" size={22}/>
           </View>  
           <View style={{flexDirection:'row' ,paddingBottom:10}}>
               <Text style={styles.text}>
                   Нэр: {ordername}
               </Text>
           </View>
           <View style={{flexDirection:'row' ,paddingBottom:10}}>
               <Text style={styles.text}>
                   Утас: {phone}
               </Text>
           </View>
         </View>

        <View style={styles.header}>
          <Text style={{fontSize: 18, color: '#493F50'}} onPress={()=>console.log()}>Барааны мэдээлэл </Text>
        </View>  
        {
          product.map((item, index)=>{
            return( 
              <View style={{flexDirection:'row', backgroundColor:'white', padding:10, marginBottom:8}}>
              <Image
                source={image}
                style={{
                  height:60,
                  width:70
                }}
              />
              <View style={{flexDirection:'column', paddingLeft: 15,  marginLeft:10, marginRight:10, width:'70%'}}>
                 <Text style={{fontSize:18, color:'black'}}>{item.pname} </Text>
                 <Text style={{fontSize:18, paddingRight: 20}}>Ширхэг: {item.quant}</Text>
              </View>
            </View>
            );
          })
        }   
         <View style={{padding:10, backgroundColor:'white'}}>
                  <Text style={{color:'black', fontSize:18}}>Нийт төлөх дүн: ₮{tprice}</Text>
         </View>
      </View>
    );
};

export default OrderDetail;

const styles = StyleSheet.create({
  container: {
    padding: 17,
    flex: 1, 
    alignItems: 'flex-start', 
    justifyContent: 'flex-start',
  },
  header:{
    flexDirection: 'row',
    paddingBottom:10,
    
  },
  text:{
    fontSize: 16,
    color: 'black',
    paddingRight:10
  }
});