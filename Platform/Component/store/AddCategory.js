import React, {useState} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button,
    TouchableOpacity,
    ImageBackground,
    Alert,
} from 'react-native';
//import { Icon } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';
import Icon from 'react-native-vector-icons/MaterialIcons'; 
import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import {AuthContext} from '../navigation/AuthProvider';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';
const AddCategory = ({navigation})=>{
    const [category_name,setCategory] = useState();

    const CreateCategory = () => {
       database()
        .ref(`category/${category_name}`)
        .set(category_name)
        .then(() => Alert.alert(category_name,'Амжилттай'));
    }
   
    return(
   <View style={styles.container}>
       <Text style={styles.textt } onPress = {()=> category}>Ангилалын нэр оруулна уу:</Text>
       <View style={{padding:10,margin:3}}>
          <FormInput  
            labelValue={category_name}
            onChangeText={(name)=>setCategory(name)}
            placeholderText='Ангилалын нэр оруул'
          />
       </View>
      <FormButton 
        buttonTitle = 'Ангилал үүсгэх'
        onPress={()=>CreateCategory()}
      />
   </View>
    );
}
export default AddCategory;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding:10,
        margin: 10,
    },
    textt:{
        fontSize: 20,
        color:'black',
        paddingLeft:5,
        fontWeight: 'bold',
        
    },
    text:{
        fontSize: 16,
        color:'black',
        paddingLeft:5,
        width:'90%'

    },
    signIn: {
    width: 140,
    height: 50,
    color:'#000000',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    flexDirection:'row',
    backgroundColor:'#08AAC1',
    
    
},

})