import React from 'react';


import MainTabStore from './MainTabStore';
import CreateStore from './CreateStore';
import { createStackNavigator } from '@react-navigation/stack';

const StoreStack  = createStackNavigator();

const StoreStackScreen = ({navigation}) => (
  <StoreStack.Navigator headerMode='CreateStore'>
      <StoreStack.Screen name="CreateStore" component={CreateStore}/>
      <StoreStack.Screen name="MainTabStore" component={MainTabStore}/>
  </StoreStack.Navigator>
);

export default StoreStackScreen;