import React, {useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Button,
    TouchableOpacity,
    ImageBackground,
    Alert,
} from 'react-native';
//import { Icon } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'; 
import LinearGradient from 'react-native-linear-gradient';

import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import {AuthContext} from '../navigation/AuthProvider';
import AddProduct from './AddProduct';
import database from '@react-native-firebase/database';
const images  = require('../assets/man.png');
const EditProduct = ({navigation, route})=>{
  
  const [pcode, setCode] = useState();
  const [price, setPrice] = useState();
  const [quant, setQuant] = useState();
  const [comment, setComment] = useState();
  const [pro_name, setName] = useState(route.params);
  const aa = route.params;
  const value = {
    productname: pro_name,
    productcode: pcode,
    price: price,
    quantity: quant,
    description: comment
  }
  const update_p = () => {
    database()
      .ref(`product/${pro_name}`)
      .update(value)
      .then(()=> Alert.alert("Бараа","Амжилттай шинэчлэгдлээ"));
  }
  const delete_p = () =>{
    database()
      .ref(`product/${pro_name}`)
      .remove()
      .then(res => {
        Alert.alert('Бараа','Амжилттай устгагдлаа');
      })
  }
    return(
   <View style={styles.container}>
       <TouchableOpacity onPress={() => {}} >
                <View
                   style={{
                     height: 100,
                     width: 100,
                     borderRadius: 15,
                     justifyContent:'center',
                     alignItems: 'center',
                   }}
                 >
                   <ImageBackground
                     source={images}
                    style={{height: 70, width: 70}}
                    imageStyle={{borderRadius: 15}}
                    >
                      <View style={{
                          flex: 1,
                          justifyContent: 'center',
                          alignItems: 'center',
                      }}>
                         <Icon name="camera" size={35} color="#fff" style={{
                            opacity: 0.7,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderWidth: 1,
                            borderColor: '#fff',
                            borderRadius: 10,
                         }}/>
                      </View>
                    </ImageBackground>
                 </View>
             </TouchableOpacity>
       
       <FormInput
            labelValue={pro_name}
            onChangeText={(pro_name) => setName(pro_name)}
            placeholderText="Барааны нэр"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormInput
      
            labelValue={pcode}
            onChangeText={(pcode) => setCode(pcode)}
            placeholderText="Барааны код"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormInput
            labelValue={price}
            onChangeText={(price) => setPrice(price)}
            placeholderText="Үнэ"
            keyboardType="numeric"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormInput
            labelValue={quant}
            onChangeText={(quant) => setQuant(quant)}
            placeholderText="Тоо ширхэг"
            keyboardType="numeric"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormInput
            height={80}
            labelValue={comment}
            onChangeText={(comment) => setComment(comment)}
            placeholderText="Тайлбар"
            autoCapitalize="none"
            autoCorrect={false}
      />
      <FormButton
        buttonTitle="Хадгалах"
        onPress={()=> update_p()}
      />
      <TouchableOpacity style={[styles.signin],{paddingTop:10}} onPress={()=> delete_p()} >
      <LinearGradient
        colors={['#D31818', '#FF3D3D']}
        style={styles.signin}
      >
        <Text style={[styles.textSign, {
                        color:'#fff'
                    }]}>Устгах</Text>
      </LinearGradient>
    </TouchableOpacity>
   </View>
    );
}
export default EditProduct;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding:10,
        margin: 10,
    },
    textt:{
        fontSize: 20,
        color:'black',
        paddingLeft:5,
        fontWeight: 'bold',
        
    },
    text:{
        fontSize: 16,
        color:'black',
        paddingLeft:5,
        width:'90%'

    },
    signIn: {
    width: 140,
    height: 50,
    color:'#000000',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    flexDirection:'row',
    backgroundColor:'#08AAC1',
    
    
    },
    signin: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    }

})