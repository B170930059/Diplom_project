import React, { Component } from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from './SplashScreen';
import SignInScreen from './SignInScreen';
import SignUpScreen from './SignUpScreen';
import Login from './Login';
import Register from './Register';
import MainTab from './MainTabScreen';
import MainStore from '../store/MainTabStore';
import CreateStore from '../store/CreateStore';
import Store from '../store/Store';
import StoreDetails from '../store/StoreDetails';
import NavigationContain from '../storelist/NavigationContainer';
const RootStack = createStackNavigator();

const RootStackScreen=()=>{

      return( 
        <RootStack.Navigator headerMode='Navigation'>
            <RootStack.Screen name="SplashScreen" component={SplashScreen}/>
            <RootStack.Screen name="SignInScreen" component={SignInScreen}/>
            <RootStack.Screen name="SignUpScreen" component={SignUpScreen}/>
            <RootStack.Screen name="Login" component={Login}/>
            <RootStack.Screen name="SignUp" component={Register}/>
            <RootStack.Screen name="MainTab" component={MainTab}/>
            <RootStack.Screen name="MainStore" component={MainStore}/>
            <RootStack.Screen name="Navigation" component={NavigationContain}/>
            <RootStack.Screen name="CreateStore" component={CreateStore}/>
            <RootStack.Screen name="StoreDetails" component={StoreDetails}/>
            {/* <RootStack.Screen name="Store" component={Store}/> */}
        </RootStack.Navigator>
  );
}
export default RootStackScreen;