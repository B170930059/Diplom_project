import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  StyleSheet,
  Button
} from 'react-native';

import {useTheme} from 'react-native-paper';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import Animated from 'react-native-reanimated';
import { colors } from 'react-native-elements';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import firestore from '@react-native-firebase/firestore';
const images  = require('../assets/man.png');
  
  const EditProfileScreen = () =>{
   
    const [fname, setFname] = useState();
    const [lname, setLname] = useState();
    const [phone, setPhone] = useState();
    const [address, setAddress] = useState();
    const [email, setEmail] = useState();
    
    useEffect(()=>{
      try{
        AsyncStorage.getItem('email').then((emailadd)=>{
           if(emailadd !== null){
              setEmail(emailadd);
           }
        })  
      }
      catch(err){
        console.log(err);
      }
    })
    const update = () => {
      const value = {
        email: email,
        lname: lname,
         fname: fname,
         phone: phone,
         address: address
      }
      database()
        .ref(`users/${phone}`)
        .update(value)
        .then(() => alert('Амжилттай хадгаллаа'));
      
      try{
        const val = {
          name: fname,
          phone: phone,
        }
        AsyncStorage.setItem('user', JSON.stringify(val));
      }
      catch(er){
        console.log(er);
      }
    }
    
    return (
     <View style={styles.container}> 
      
      <View style={{margin: 20}}>
         <View style = {{alignItems:'center'}}>
             <TouchableOpacity onPress={() => {}}>
                <View
                   style={{
                     height: 100,
                     width: 100,
                     borderRadius: 15,
                     justifyContent:'center',
                     alignItems: 'center',
                   }}
                 >
                   
                   <ImageBackground
                     source={images}
                    style={{height: 100, width: 100}}
                    imageStyle={{borderRadius: 15}}
                    >
                      <View style={{
                          flex: 1,
                          justifyContent: 'center',
                          alignItems: 'center',
                      }}>
                         <Icon name="camera" size={35} color="#fff" style={{
                            opacity: 0.7,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderWidth: 1,
                            borderColor: '#fff',
                            borderRadius: 10,
                         }}/>
                      </View>
                    </ImageBackground>
                 </View>
             </TouchableOpacity>
            <Text style={{marginTop: 10, fontSize: 30, fontWeight: 'bold'}} >
                {fname}
            </Text>
         </View> 
         <View style={styles.action}>
             <FontAwesome name="user-o" color={colors.text} size={20}/>
             <TextInput 
               //value={fname} 
               onChangeText={(lname) => setLname(lname)}
               placeholder="Овог"
               placeholderTextColor="#666666"
               autoCorrect={false}
               style={styles.textInput}
               />
         </View>  
         <View style={styles.action}>
             <FontAwesome name="user-o" color={colors.text} size={20}/>
             <TextInput 
              onChangeText={(fname) => setFname(fname)}
               placeholder="Нэр"
               placeholderTextColor="#666666"
               autoCorrect={false}
               style={styles.textInput}
               />
         </View>  
         <View style={styles.action}>
             <Feather name="phone" color={colors.text} size={20}/>
             <TextInput 
               onChangeText={(phone) => setPhone(phone)}  
               placeholder="Утасны дугаар"
               placeholderTextColor="#666666"
               autoCorrect={false}
               style={styles.textInput}
               />
         </View>  
         <View style={styles.action}>
             <Feather name="map-pin" color={colors.text} size={20}/>
             <TextInput 
               onChangeText={(address)=> setAddress(address)}
               placeholder="Гэрийн хаяг"
               placeholderTextColor="#666666"
               autoCorrect={false}
               style={styles.textInput}
               />
         </View>  
         <TouchableOpacity style={styles.commandButton} onPress={() => update()}>
           <Text style={styles.panelButtonTitle}>Хадгалах</Text>
         </TouchableOpacity>
      </View>
     </View>
    )

  }

export default EditProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  commandButton: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: '#D0C402',
    alignItems: 'center',
    marginTop: 10,
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    // borderTopLeftRadius: 20,
    // borderTopRightRadius: 20,
    // shadowColor: '#000000',
    // shadowOffset: {width: 0, height: 0},
    // shadowRadius: 5,
    // shadowOpacity: 0.4,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    // elevation: 5,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
});