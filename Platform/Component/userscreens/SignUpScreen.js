import React, {useContext, useState, createContext} from 'react';

import auth from '@react-native-firebase/auth';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    TextInput,
    Platform,
    StyleSheet ,
    StatusBar,
    Alert,
    ToastAndroid
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useTheme } from 'react-native-paper';

//import { AuthContext } from '../navigation/AuthProvider';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';

const SignUpScreen = ({navigation}) => {

   // const [name, setName]  = useState();
    const [email, setEmail]  = useState();
    const [password, setPassword] = useState();
    const { colors } = useTheme();

    const Register = () => {
        auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          console.log('User account created');
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            console.log('That email address is already in use!');
          }
      
          if (error.code === 'auth/invalid-email') {
            console.log('That email address is invalid!');
          }
      
          console.error(error);
        });
        Alert.alert('Бүртгэл амжилттай үүслээ')
        navigation.navigate('CreateStore', {
            useremail: email,
        });
      }
    return (
      <View style={styles.container}>
          <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View style={styles.header}>
            <Text style={styles.text_header}>Бүртгүүлэх</Text>
        </View>
        <Animatable.View 
            animation="fadeInUpBig"
            style={[styles.footer, {
                backgroundColor: colors.background
            }]}
        >
            <Text style={[styles.text_footer, {
                color: '#3E3E3E'
            }]}>И-мэйл хаяг</Text>
            <View style={styles.action}>
                <FormInput
                    labelValue={email}
                    onChangeText={(email) => setEmail(email)}
                    placeholderText="И-мэйл хаяг"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                />
            </View>
            <Text style={[styles.text_footer, {
                color: '#3E3E3E',
                marginTop: 5
            }]}>Нууц үг</Text>
            <View style={styles.action}>
                <FormInput
                    labelValue={password}
                    onChangeText={(userpassword) => setPassword(userpassword)}
                    placeholderText="Нууц үг"
                   // iconType="lock"
                    keyboardType="password"
                    secureTextEntry={true}
                />
               
                {/* <TouchableOpacity
                    onPress={updateSecureTextEntry}
                >
                    {data.secureTextEntry ? 
                    <Feather 
                        name="eye-off"
                        color="grey"
                        size={20}
                    />
                    :
                    <Feather 
                        name="eye"
                        color="grey"
                        size={20}
                    />
                    }
                </TouchableOpacity> */}
            </View>
            <View style={styles.button}>
                <FormButton
                    buttonTitle="Бүртгүүлэх"
                    onPress={() => Register()}
                 />
                {/* <TouchableOpacity
                    style={styles.signIn}
                    onPress={() => login(email, password)}
                >
                <LinearGradient
                    colors={['#7207B3', '#01ab9d']}
                    style={styles.signIn}
                >
                    <Text style={[styles.textSign, {
                        color:'#fff'
                    }]}>Нэвтрэх</Text>
                </LinearGradient>
                </TouchableOpacity>
                 */}
                <View style={{flexDirection:'row', paddingTop:8 }}>
                    <TouchableOpacity
                        style={[styles.signIn, {
                            borderColor:'#D0C402',
                            borderWidth:1,
                            margin: 15,    
                        }]}
                        onPress={() => navigation.navigate('SignInScreen')}>
                        <Text style={[styles.textSign, {
                            color: '#9E9E9E'
                        }]}>Нэвтрэх</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Animatable.View>
      </View>
    );
};

export default SignUpScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#D0C402'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 20
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    }
  });