import { Navigation } from '@material-ui/icons';
import { createStackNavigator } from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import React, { useContext, useEffect, useState} from 'react';
import {View, SafeAreaView, StyleSheet, TouchableOpacity} from 'react-native';
import {
  Avatar,
  Title,
  Caption,
  Text,
  TouchableRipple,
  Button
} from 'react-native-paper';

import Icon from 'react-native-vector-icons/MaterialIcons';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database'; 
const image  = require('../assets/man.png');


const ProfileScreen = ({navigation}) => {
  const [name, setName] = useState();
  const [phone, setPhone] = useState();

  useEffect(()=>{
     try{
      const ref = database().ref('/users');
      const listener = ref.on('value', data=>{
        data.forEach(item=>{
          const val = item.val().fname;
          const val1 = item.val().phone;
          setName(val);
          setPhone(val1);
        })
      });
      return() =>{
        ref.off('value', listener);
       }
     }
     catch(er){
       console.log(er);
     }
  })
   const exit = () =>{
    auth()
      .signOut()
      .then(()=>{
        console.log('User signed out!');
      });
      navigation.navigate('SplashScreen');
  }


  return (
    
    <SafeAreaView style={styles.container}>
    
      <View style={styles.userInfoSection}>
        <View style={{flexDirection: 'row', marginTop: 15}}>
          <Avatar.Image 
            source={image}
            size={80}
          />
          <View style={{marginLeft: 20}}>
            <Title style={[styles.title, {
              marginTop:15,
              marginBottom: 5,
            }]}>{name}</Title>
            <Caption style={styles.caption} onPress={()=>console.log(name)}>{phone}</Caption>
          </View>
        </View>
      </View>

      <View style={styles.userInfoSection}>
        <View style={styles.row}>
          <Icon name="location-on" color="#777777" size={20}/>
          <Text style={{color:"#777777", marginLeft: 20}}>Ulaanbaatar</Text>
        </View>
        <View style={styles.row}>
          <Icon name="phone" color="#777777" size={20}/>
          <Text style={{color:"#777777", marginLeft: 20}}>95608243</Text>
        </View>
        <View style={styles.row}>
          <Icon name="email" color="#777777" size={20}/>
          <Text style={{color:"#777777", marginLeft: 20}}>ela@email.com</Text>
        </View>
        <View style={styles.row}>
          <Icon name="location-city" color="#777777" size={20}/>
          <Text style={{color:"#777777", marginLeft: 20}}>Хаяг</Text>
        </View>
      </View>

      <View style={styles.infoBoxWrapper}>
          <View style={[styles.infoBox, {
            borderRightColor: '#dddddd',
            borderRightWidth: 1
          }]}>
            <Title>150K</Title>
            <Caption>Нийт зарлага</Caption>
          </View>
          <View style={styles.infoBox}>
            <Title>12</Title>
            <Caption>Нийт захиалга</Caption>
          </View>
      </View>

      <View style={styles.menuWrapper}>
        <TouchableRipple onPress={() => {}}>
          <View style={styles.menuItem}>
            <Icon name="lock-outline" color="#FF6347" size={25}/>
            <Text style={styles.menuItemText}>Нууц үг солих</Text>
          </View>
        </TouchableRipple>
        <TouchableRipple onPress={() => exit()}>
          <View style={styles.menuItem}>
            <Icon name="exit-to-app" color="#FF6347" size={25}/>
            <Text style={styles.menuItemText}>Гарах</Text>
          </View>
        </TouchableRipple>
        
      </View>
    </SafeAreaView>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  userInfoSection: {
    paddingHorizontal: 30,
    paddingTop: 20,
    marginBottom: 25,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: '#dddddd',
    borderBottomWidth: 1,
    borderTopColor: '#dddddd',
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 100,
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
});