import React, {useContext, useState} from 'react';
import {View, Alert, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import {AuthContext} from '../navigation/AuthProvider';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';

const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState();
 // const {login} = useContext(AuthContext);
 function login(){
  let user = true;
  auth()
  .signInWithEmailAndPassword(email, password)
  .then(() => {
    console.log('User logged in your app');
    //user = true; 
  })
  .catch(error => {
    console.error(error);
    user = false;
  });
  if(user == false){
      Alert.alert('Нэвтрэх мэдээлэл алдаатай!');
  }
  else{
   navigation.navigate('MainTab');
   AsyncStorage.setItem('email', email);
  }

 // Alert.alert('Бүртгэл амжилттай үүслээ')

}
 return (
    <View style={styles.container}>
      <Text style={styles.text}>Нэвтрэх</Text>
 
      <FormInput
        labelValue={email}
        onChangeText={(email) => setEmail(email)}
        placeholderText="И-мэйл хаяг"
        keyboardType="email-address"
        autoCapitalize="none"
        autoCorrect={false}
      />
        <FormInput
        labelValue={password}
        onChangeText={(password) => setPassword(password)}
        placeholderText="Нууц үг"
        secureTextEntry={true}
      />
 
      <FormButton
        buttonTitle="Нэвтрэх"
        onPress={() =>  {
          if(!email.trim()){
            Alert.alert("Email хаяг оруулна уу"); 
           }
          else{
            login()
         }
        }
      }
      />
 
      <TouchableOpacity style={styles.forgotButton} onPress={() => navigation.navigate('SignInScreen')}>
        <Text style={styles.navButtonText}>Нууц үг мартсан</Text>
      </TouchableOpacity>
 
      <TouchableOpacity
        onPress={() => navigation.navigate('SignUp')}
        >
        <Text style={styles.navButtonText}>
        Бүргүүлэх бол энд дарна уу! 
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default LoginScreen;
 
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f9fafd',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  logo: {
    height: 150,
    width: 150,
    resizeMode: 'cover',
  },
  text: {
    fontFamily: 'Kufam-SemiBoldItalic',
    fontSize: 28,
    marginBottom: 10,
    color: '#051d5f',
  },
  navButton: {
    marginTop: 15,
  },
  forgotButton: {
    marginVertical: 15,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#2e64e5',
    fontFamily: 'Lato-Regular',
  },
});