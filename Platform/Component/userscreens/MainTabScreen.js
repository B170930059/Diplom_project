import React, {useContext} from 'react';

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import HomeScreen from './HomeScreen';
import OrdersScreen from './OrdersScreen';
import ProfileScreen from '../storelist/ProfileScreen'
import Profile_ from '../userscreens/ProfileScreen';
import { useTheme } from '@react-navigation/native';
import EditProfileScreen from './EditProfileScreen';
import OrderDetailsScreen from '../storelist/OrderDetailsScreen';
import StoreList from '../storelist/Store';
import Product from '../storelist/Product';
import Cart from '../storelist/Cart';
import Order from '../storelist/Order';
import Bookmark from '../storelist/Bookmark';

import {AuthContext} from '../navigation/AuthProvider';
import { Alert } from 'react-native';
const HomeStack = createStackNavigator();
const OrdersStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();
 
const MainTabScreen = () => {
  return(
  <Tab.Navigator
      initialRouteName="Home"
      activeColor="#2E2E2E"
      inactiveColor="#ABABAB"
      barStyle={{ backgroundColor: '#fff' }}
    >
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarLabel: 'Эхлэх',
          tabBarColor: 'black',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-home" color={color} size={26} />
          ),
        }}
        
      />
      
      <Tab.Screen
        name="Orders"
        component={OrdersStackScreen}
        options={{
          tabBarLabel: 'Захиалга',
          tabBarColor: '#7207B3',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-notifications" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStackScreen}
        options={{
          tabBarLabel: 'Тохиргоо',
          tabBarColor: '#7207B3',
          tabBarIcon: ({ color }) => (
            <Icon name="ios-person" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default MainTabScreen;

const HomeStackScreen = ({navigation, route}) => {
  //const {name} = route.params;
    return(
      <HomeStack.Navigator 
        screenOptions={{
          headerStyle: {
          backgroundColor: '#DCD802',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          alignSelf: 'flex-start'
          },
          // headerLeft: () => (
          //   <Icon.Button
          //     name='ios-menu'
          //     size={25}
          //     backgroundColor='#7207B3'
          //     color='#fff'
          //     onPress={()=> navigation.openDrawer()}
          //   />
          // ),
    }}>
      
      <HomeStack.Screen 
        name="Home" 
        component={HomeScreen} 
        
        options={{
        title:'Дэлгүүрийн жагсаалт',
        headerTitleAlign:'center',
        headerTintColor:'#3D3D3D',
        headerStyle:{
          backgroundColor:'#fff',
        },
        headerLeft:()=> null,
        }}
        />
        
        <HomeStack.Screen 
        name="StoreList" 
        component={StoreList} 
        options={({route}) =>({
        title: route.params.storename,
        
        headerTitleAlign:'center',
        headerTintColor:'black',
        headerStyle: {
          backgroundColor: 'white',
          },
        headerLeft:()=>(
          <Icon.Button 
              name="ios-menu"
              size={26}
              backgroundColor='white'
              color='#4B4B4B'
              onPress={()=> navigation.openDrawer()}
          />
        ),
        headerRight:()=>(
          <Icon.Button
             name="cart-outline"
             size={26}
             backgroundColor='white'
             color='#4B4B4B'
             onPress={()=> navigation.navigate('Cart')}
          />
        ),
        
        })
      } 
        />
        <HomeStack.Screen 
        name="Cart" 
        component={Cart} 
        options={{
        title:'Сагс',
        headerTitleAlign:'center',
        headerTintColor:'black',
        headerStyle:{
          backgroundColor:'white'
        }
        }} />
        <HomeStack.Screen 
        name="Product" 
        component={Product} 
        options={{
        title:'Барааны мэдээлэл',
        headerTitleAlign:'center',
        headerTintColor:'black',
        headerStyle:{
          backgroundColor:'white'
        }
        }} />

      <HomeStack.Screen 
        name="ProfileScreen" 
        component={ProfileScreen} 
        options={{
        title:'Хэрэглэгчийн булан',
        headerTitleAlign:'center',
        headerTintColor:'black',
        headerStyle:{
          backgroundColor:'white'
        }
        }} />
        <HomeStack.Screen 
        name="OrderScreen" 
        component={Order} 
        options={{
        title:'Захиалгын мэдээлэл',
        headerTitleAlign:'center',
        headerTintColor:'black',
        headerStyle:{
          backgroundColor:'white'
        }
        }} />
        <HomeStack.Screen 
        name="OrderDetail" 
        component={OrderDetailsScreen} 
        options={{
        title:'Захиалгын мэдээлэл',
        headerTitleAlign:'center',
        headerTintColor:'black',
        headerStyle:{
          backgroundColor:'white'
        }
        }} />
        <HomeStack.Screen 
        name="Bookmark" 
        component={Bookmark} 
        options={{
        title:'Хадгалсан бараа',
        headerTitleAlign:'center',
        headerTintColor:'black',
        headerStyle:{
          backgroundColor:'white'
        }
        }} />
      </HomeStack.Navigator>
    )
}
  
const OrdersStackScreen = ({navigation}) => {
    return(
      <OrdersStack.Navigator
    screenOptions={{
    headerStyle: {
    backgroundColor: '#DCD802',
    },
    headerTintColor:'#3D3D3D',
        headerStyle:{
          backgroundColor:'#fff',
    }
}}>
    <OrdersStack.Screen
      name="OrderScreen"
      component={OrdersScreen}
      options={{
         title: 'Захиалгын түүх',
         headerTitleAlign:'center',
         headerLeft:()=> null,
      }}
    />
    
  </OrdersStack.Navigator>
  );
}
const ProfileStackScreen = ({navigation}) => {
    return(
      <ProfileStack.Navigator 
      screenOptions={{
        headerStyle: {
        backgroundColor: '#fff',
        },
        headerTintColor: '#3D3D3D',
        headerTitleStyle: {
        fontWeight: 'bold',
        alignSelf: 'flex-start',
        shadowColor: '#fff'
        }
    }}>
        <ProfileStack.Screen 
        name="Profile" 
        component={Profile_} 
        options={{ title: 'Хувийн мэдээлэл',
        headerTitleAlign:'center',
        headerTintColor:"#3D3D3D",
        headerStyle:{
          backgroundColor:"#fff",
        },
        headerLeft:()=> null,
        headerRight: () => (
            <MaterialCommunityIcons.Button
              name='account-edit'
              size={25}
              backgroundColor='#fff'
              color='#3D3D3D'
              onPress={()=> navigation.navigate('EditProfile')}
            />
        ),
        }} />
      <ProfileStack.Screen
        name='EditProfile'
        options={{
          title: 'Профайл засах'
        }}
        component={EditProfileScreen}
      />
</ProfileStack.Navigator>
    )
}