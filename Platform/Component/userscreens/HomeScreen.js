import React, {useContext, useEffect, useState} from 'react';
import { View, Text, Button, StyleSheet, StatusBar, ScrollView, TouchableOpacity, Alert } from 'react-native';
import Searchbar from './SearchbarScreen';

import { AuthContext } from '../navigation/AuthProvider';
import FormButton from '../components/FormButton';
import  {Avatar} from 'react-native-paper';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Item } from 'react-native-paper/lib/typescript/components/List/List';

const image = require('../assets/shop1.png');
const image1 = require('../assets/logo1.png');
const image2 = require('../assets/logo.png');


const HomeScreen = ({navigation}) => {
  const [store, setStorelist] = useState([]);

  useEffect(()=>{ 
    try{
    const ref =  database().ref('/storelist');
    const listener = ref.on('value', (snapshot)=> {
      
      let temp = [];
      snapshot.forEach((doc) => {
           let data = doc.val().storename;
           temp.push(data);
        }); 
      setStorelist(temp);
    });
    return () =>{
      ref.off('value', listener);
    }
    }
    catch(err){
      console.log(err);
    }
 });
  const getuserinfo = (data) => {
    Alert.alert(
      'Нэр, Утасны дугаар',
      'Таны дээрх хоёр мэдээллийг ашиглана!',
      [
        {
          text: 'Зөвшөөрөх',
          onPress:()=> navigation.navigate('StoreList',{
            storename: data,
          }  ),
        },
        {
          text: 'Үгүй',
          onPress:()=>navigation.navigate('Home'),
        }
      ]
      )
 }
 
  return (
    <View style={styles.container}>
      {/* <Text onPress={()=> display()}>Click me</Text> */}
      <Searchbar/>
      {
         store.map((item, index) => {
          return(    
          <View style={{paddingLeft:8,paddingRight:8,marginBottom:10, width:'100%'}}>
                <TouchableOpacity  onPress={()=> getuserinfo(item)} 
                    style={{backgroundColor: '#F4F4EE', width:'100%',padding:5,paddingTop:0}}>
                  <View style={{flexDirection:'row', paddingTop: 10}}>
                      <Avatar.Image 
                          source={image}
                          style={{backgroundColor:'white'}}
                          size={50}
                      />
                      <View style={{flexDirection:'column'}}>
                          <Text style={styles.text} key={index}>
                           {item}
                          </Text>
                          <Text style={styles.text} >
                              75754545
                          </Text>     
                      </View>    
                    </View>
                  </TouchableOpacity>    
                </View>   
          );
      })
      }
      
      
    </View>
  );
}
export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#E5E5E0',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingTop: 4,
    padding:8
  },
  text:{
    fontSize: 18,
    paddingLeft: 13,
    flexDirection:'column',
    color: '#493F50',
  }

});