import React, { useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import {
    useTheme,
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';

import Icon from 'react-native-vector-icons/MaterialIcons';


const image = require('../assets/man.png');
export function DrawerContent(
    props) {

    const paperTheme = useTheme();

    //const {user, logout} = useContext(AuthContext);

    return(
        <View style={{flex:1}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                  <Title style={styles.title} onPress={()=> props.navigation.navigate('Home')}>Нүүр</Title>
                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem 
                            labelStyle={{
                                fontSize:16,
                            }}                        
                            label="Технологи"
                            onPress={() => {props.navigation.navigate('Home')}}
                        />
                        <DrawerItem 
                           labelStyle={{
                            fontSize:16
                             }}  
                            label="Спорт"
                            onPress={() => {props.navigation.navigate('StoreList')}}
                        />
                       
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem 
                    icon={({color, size}) => (
                        <Icon 
                        name="account-circle" 
                        color={color}
                        size={26}
                        />
                    )}
                    label="Хэрэглэгчийн булан"
                    labelStyle={{fontSize:16,}}
                    onPress={() => props.navigation.navigate('ProfileScreen')}
                />
            </Drawer.Section>
        </View>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
      padding:5
      //borderTopRightRadius:10
    },
    userInfoSection: {
      //paddingLeft: 20,
    },
    title: {
      fontSize: 18,
      margin:10,
      paddingLeft:8
    },
    
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    drawerSection: {
      
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
    },
  });