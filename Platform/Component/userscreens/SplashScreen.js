import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    Dimensions,
    StyleSheet,
    StatusBar,
    Image,
    ImageBackground
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/Ionicons';
import { useTheme } from '@react-navigation/native';

const image =  require('../assets/shop1.png');
const SplashScreen = ({navigation}) => {
    const { colors } = useTheme();
  
    return (
      <View style={styles.container}>
         <ImageBackground source={image} style={styles.image}> 
           <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View style={styles.header}>
        <Text style={styles.texttitle}>Хэрэглэгчийн төрөл сонгоно уу!</Text>
        
            <View style={styles.button}>
                <TouchableOpacity style={{paddingRight:16}} onPress={()=>navigation.navigate('Login')}>
                    <LinearGradient
                        colors={['#D0C402', '#D0C402']}
                        style={styles.signIn}
                    >
                        <Text style={styles.textSign}>Хэрэглэгч</Text>
                        <MaterialIcons 
                            name="navigate-next"
                            color="#646464"
                            size={20}
                        />
                    </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>navigation.navigate('SignInScreen')}>
                    <LinearGradient
                        colors={['#D0C402', '#D0C402']}
                        style={styles.signIn}
                    >
                        <Text style={styles.textSign}>Админ</Text>
                        <MaterialIcons 
                            name="navigate-next"
                            color="#646464"
                            size={20}
                        />
                    </LinearGradient>
                </TouchableOpacity>
            </View>
           
        </View> 
        </ImageBackground>
        {/* <Animatable.View 
            style={[styles.footer, {
                backgroundColor: colors.background
            }]}
            animation="fadeInUpBig"
        >
            <Text style={[styles.title, {
                color: colors.text
            }]}>Онлайн худалдааны платформд тавтай морил</Text>
            <Text style={styles.text}>Нэвтрэхийн хүсвэл Эхлэх товчийг дарна уу</Text>
            <View style={styles.button}>
            <TouchableOpacity onPress={()=>navigation.navigate('SignInScreen')}>
                <LinearGradient
                    colors={['#08d4c4', '#01ab9d']}
                    style={styles.signIn}
                >
                    <Text style={styles.textSign}>Эхлэх</Text>
                    <MaterialIcons 
                        name="navigate-next"
                        color="#fff"
                        size={15}
                    />
                </LinearGradient>
            </TouchableOpacity>
            </View>
        </Animatable.View> */}
      </View>
    );
};

export default SplashScreen;

const {height} = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#FCF55F',
    
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  header: {
      flex: 2,
      justifyContent: 'flex-end',
      alignItems: 'center',
      paddingEnd: 20,
      paddingBottom: 10,
      flexDirection: 'column'
  },
  footer: {
      flex: 1,
      backgroundColor: '#fff',
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20,
      paddingVertical: 50,
      paddingHorizontal: 30
  },
  logo: {
      width: height_logo,
      height: height_logo
  },
  title: {
      color: '#05375a',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: 'grey',
      marginTop:5
  },
  button: {
      alignItems: 'flex-end',
      marginTop: 8,
      flexDirection: 'row',
     // justifyContent: "flex-start",
      

  },
  signIn: {
      width: 140,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 50,
      flexDirection: 'row'
  },
  textSign: {
      color: '#646464',
      fontWeight: 'bold',
      fontSize: 17
  },
  texttitle: {
    color: '#646464',
    fontWeight: 'bold',
    fontSize: 18,
}
});