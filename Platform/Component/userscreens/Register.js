import React, {useContext, useState} from 'react';
import {View, Alert, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import {AuthContext} from '../navigation/AuthProvider';
import auth from '@react-native-firebase/auth';


const SignupScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState();
  const Register = () => {
    auth()
    .createUserWithEmailAndPassword(email, password)
    .then(() => {
      console.log('User account created');
    })
    .catch(error => {
      if (error.code === 'auth/email-already-in-use') {
        console.log('That email address is already in use!');
      }
  
      if (error.code === 'auth/invalid-email') {
        console.log('That email address is invalid!');
      }
  
      console.error(error);
    });
    Alert.alert('Бүртгэл амжилттай үүслээ')
    navigation.navigate('Login');
  }
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Бүргүүлэх</Text>
 
      <FormInput
        labelValue={email}
        onChangeText={(email) => setEmail(email)}
        placeholderText="И-мэйл хаяг"
        keyboardType="email-address"
        autoCapitalize="none"
        autoCorrect={false}
      />
      <FormInput
        labelValue={password}
        onChangeText={(userPassword) => setPassword(userPassword)}
        placeholderText="Нууц үг"
        secureTextEntry={true}
      />
      
    
      <FormButton
        buttonTitle="Бүртгүүлэх"
        onPress={() => Register()}
      />
      <TouchableOpacity
        style={styles.navButton}
        onPress={() => navigation.navigate('Login')}>
        <Text style={styles.navButtonText}>
          Бүртгэлтэй бол энд дарна уу!
        </Text>
      </TouchableOpacity>
    </View>
  );
};
 
export default SignupScreen;
 
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f9fafd',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  text: {
    fontFamily: 'Kufam-SemiBoldItalic',
    fontSize: 28,
    marginBottom: 10,
    color: '#051d5f',
  },
  navButton: {
    marginTop: 15,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#2e64e5',
    fontFamily: 'Lato-Regular',
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 35,
    justifyContent: 'center',
  },
  color_textPrivate: {
    fontSize: 13,
    fontWeight: '400',
    fontFamily: 'Lato-Regular',
    color: 'grey',
  },
});