import React from 'react';
import { View, Text, Button, StyleSheet, Touchable, Alert, Picker } from 'react-native';
import { fonts } from 'react-native-elements/dist/config';
import { TouchableOpacity } from 'react-native-gesture-handler';
import  {Avatar} from 'react-native-paper';
//import {Dropdown} from 'react-native-material-dropdown';
import ModalDropdown from 'react-native-modal-dropdown';
const image = require('../assets/shop1.png');

const OrdersScreen = ({navigation}) => {
 
  return (
      <View style={styles.container}>
        <View style={styles.header}>
           <Text style={{fontSize: 18, color: '#493F50'}}>Нийт захиалга:</Text>
           <Text style={{fontSize: 18, color: '#493F50', paddingLeft:5}}>1</Text>  
           {/* <ModalDropdown style={{fontSize: 18, justifyContent:'flex-end', paddingLeft:150 }} options={['Төлбөр төлсөн', 'Хүлээгдэж буй']}> */}
{/*               
             </ModalDropdown> */}
        </View> 
         {/* <TouchableOpacity onPress={()=>navigation.navigate('OrderDetail')} > */}
         <View style={{flexDirection:'row', margin:3,padding: 10, paddingLeft:7, backgroundColor:'white', width:'100%'}}>
            <Avatar.Image 
                source={image}
                size={50}
                style={{backgroundColor:'#D9D9D9'}}
            />
            <View style={{flexDirection:'column'}} >
                <Text style={styles.text}  > 
                    E-mart
                </Text>
                <Text style={styles.text}>
                    2021.05.02
                </Text>     
            </View>    
          </View>
         {/* </TouchableOpacity>     */}
         
      </View>
    );
};

export default OrdersScreen;

const styles = StyleSheet.create({
  container: {
    padding: 17,
    flex: 1, 
    alignItems: 'flex-start', 
    justifyContent: 'flex-start'
  },
  header:{
    flexDirection: 'row',
    paddingBottom:10,
    
  },
  text:{
    fontSize: 18,
    paddingLeft: 13,
    flexDirection:'column',
    color: '#493F50',
  }
});