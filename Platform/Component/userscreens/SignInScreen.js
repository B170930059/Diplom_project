import React, {useContext, useState, useEffect} from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    TextInput,
    Platform,
    StyleSheet ,
    StatusBar,
    Alert,
    ToastAndroid
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useTheme } from 'react-native-paper';
import auth from '@react-native-firebase/auth';

import { AuthContext } from '../navigation/AuthProvider';
 
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import NavigationContain from '../storelist/NavigationContainer';
import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SignInScreen = ({navigation}) => {
    const [email, setEmail]  = useState('');
    const [password, setPassword] = useState('');
    const { colors } = useTheme();
    const [store, setStore] = useState([]);
    const [change, setChange] = useState();
    useEffect(()=>{
        try{
           const ref = database().ref('/storelist');
           const listener = ref.on('value', data=>{
            let temp = []
            data.forEach(doc =>{
                let val = doc.val()
                temp.push(val);    
              })
              setStore(temp);
           })  
           return()=>{
               ref.off('value', listener);
           }   
        }
        catch(er){
            console.log(er);
        }
    })
    function login(){
      
        auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
           for(let i = 0; i < store.length; i++){
                if(email === store[i].user.useremail ){
                    try{
                        const temp = store[i].storename;
                        const temp1 = store[i].storephone;
                        const val = {
                            storename: temp,
                            storephone: temp1
                        }
                        AsyncStorage.setItem('storename', JSON.stringify(val));
                        
                    }
                    catch(er){
                        
                        console.log(er);
                    }
                    navigation.navigate('MainStore', {
                        email: email,
                    });
                    break;
                }
                else{
                    navigation.navigate('SignInScreen');
                    console.log('Rejected');
                }
           } 
        })
        .catch(error => {
          console.error(error);
        });
    }

    return (
      <View style={styles.container}>
          <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View style={[styles.header] }>
            <Text style={styles.text_header} onPress={()=>console.log(store)}>Нэвтрэх</Text>
        </View>
        <Animatable.View 
            animation="fadeInUpBig"
            style={[styles.footer, {
                backgroundColor: colors.background
            }]}
        >
            <Text style={[styles.text_footer, {
                color: '#3E3E3E'
            }]}>И-мэйл хаяг</Text>
            <View style={styles.action}>
                <FormInput
                    labelValue={email}
                    onChangeText={(email) => setEmail(email)}
                    placeholderText="И-мэйл хаяг"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                />
            </View>
            <Text style={[styles.text_footer, {
                color: '#3E3E3E',
                marginTop: 5
            }]}>Нууц үг</Text>
            <View style={styles.action}>
                <FormInput
                    labelValue={password}
                    onChangeText={(userpassword) => setPassword(userpassword)}
                    placeholderText="Нууц үг"
                   // iconType="lock"
                    keyboardType="password"
                    secureTextEntry={true}
                />
            </View>
           
            <TouchableOpacity>
                <Text style={{color: '#3E3E3E', marginTop:5}}>Нууц үг мартсан</Text>
            </TouchableOpacity>
            <View style={styles.button}>
                <FormButton
                    buttonTitle="Нэвтрэх"
                    onPress={() => 
                        {
                        if(!email.trim()){
                         Alert.alert("Email хаяг оруулна уу"); 
                         }else{
                             login()
                         }}
                    
                }/>
                <View style={{flexDirection:'row', paddingTop:12 }}>
                    <Text style={{
                            color: '#3E3E3E',
                            paddingRight: 20,
                            fontSize: 18
                        }}>Бүргүүлэх бол энд дарна уу!</Text>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('SignUpScreen')}>
                        <Text style={[styles.textSign, {
                            color: '#3E3E3E',
                            fontWeight:'bold'
                        }]}>Бүртгүүлэх</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Animatable.View>
      </View>
    );
};

export default SignInScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#D0C402'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 20
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    }
  });