import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
//import {windowHeight, windowWidth} from '../utils/Dimentions';
import LinearGradient from 'react-native-linear-gradient';

const FormButton = ({buttonTitle, ...rest}) => {
  return (
    <TouchableOpacity style={styles.signIn} {...rest}>
      <LinearGradient
        colors={['#D0C402', '#D0C402']}
        style={styles.signIn}
      >
        <Text style={[styles.textSign, {
                        color:'#fff'
                    }]}>{buttonTitle}</Text>
      </LinearGradient>
    </TouchableOpacity>
  );
};
 
export default FormButton;
 
const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: 10,
    width: '100%',
    backgroundColor: '#2e64e5',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ffffff',
    fontFamily: 'Lato-Regular',
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    
},
textSign: {
    fontSize: 18,
    fontWeight: 'bold'
}
});