import React from 'react';
import { AuthProvider } from './AuthProvider';
import Routes from './Routes';
import NavigationContainer from '@react-navigation/native';
import NContain from '../storelist/NavigationContainer'; 
import { 
  Provider as PaperProvider
} from 'react-native-paper';

const Providers = () => {
  return (
      <AuthProvider>
        <Routes />
      </AuthProvider>
  );
}
 
export default Providers;