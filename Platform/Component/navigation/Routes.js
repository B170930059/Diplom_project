import React, {useContext, useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import {AuthContext} from './AuthProvider';
import MainTabStore from '../store/MainTabStore';
import MainTabScreen from '../userscreens/MainTabScreen';
//import AuthStack from './AuthStack';
//import StoreRoot from '../store/StoreRoot';
import RootStackScreen from '../userscreens/RootStackScreen';

import { createDrawerNavigator } from '@react-navigation/drawer';
//import MainTabStore from '../store/MainTabStore';

import { DrawerContent } from '../userscreens/DrawerContent';
//import Storelist from './Store'; 
const Drawer = createDrawerNavigator();

const Routes = (props) => {
    const {user, setUser} = useContext(AuthContext);
    const [initializing, setInitializing] = useState(true);
   
    const onAuthStateChanged = (user) => {
      setUser(user);
      if (initializing) setInitializing(false);
    };
   
    useEffect(() => {
      const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
      return subscriber; // unsubscribe on unmount
    }, []);
   
    if (initializing) return null;
   
    return (
      <NavigationContainer>
        {user ? <MainTabScreen /> : <RootStackScreen />}    
      </NavigationContainer>
    );
  };
  export default Routes;