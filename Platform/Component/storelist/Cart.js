import React, { Component, useEffect, useState } from 'react'
import { TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  StyleSheet,
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  Button,
  Alert
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import FormButton from '../components/FormButton';
import database from '@react-native-firebase/database';
import { cos } from 'react-native-reanimated';
//import Icon from 'react-native-vector-icons/Ionicons';

const image = require('../assets/airpod.png');
const Store = ()=>{
  const [dataCart, setDataCart] = useState([]);
  const [totalprice, setTprice] = useState();
  const [id, setId] = useState();
  const [date, setDate] = useState();
  const [user, setUser] = useState();
  useEffect(() => {
    try{
      AsyncStorage.getItem('cart').then((cart)=>{
        if(cart !== null){
          const cartp = JSON.parse(cart)
          setDataCart(cartp);
        }
      });
      total_price();
      // userinfo();
    }
    catch(er){
        console.log(er);
    }
  });
  
  const order = () =>{
    
     const orderdetails = {
       id: id,
       date: date,
       name: user.name,
       phone: user.phone,
       total_price: totalprice,
       
     } 
     try{
        AsyncStorage.setItem('order', JSON.stringify(dataCart));
        database().ref(`/order/${id}`).set(orderdetails);
        alert('Захиалга амжилттай');
        remove();
     }
     catch(er){
       console.log(er);
     }
  }
  
  const remove = () => {
    try{
       AsyncStorage.removeItem('cart'); 
      // alert("Сагснаас хасагдлаа");
    }
    catch(er){
      console.log(er);
    }
  }
  const total_price = () =>{
    let tp = 0;
    for(let i = 0; i < dataCart.length; i++){
         tp  = tp + dataCart[i].price;     
    }
    setTprice(tp);
  }
  const saveItem = (data) => {
    const id = data.pname;
     database()
      .ref(`/saveditems/${id}`)
      .set({
         pname: data.pname,
         price: data.price
      })
      .then(() => alert("Amjilttai hadgalagdlaa!"));
  }
  const makeid = () => {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    var hours = new Date().getHours();
    var min = new Date().getMinutes();
    setDate(month + '/' + date + '/' + year
            + ' ' + hours + ':' + min
    );

    for (let i = 0; i < 6; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));  
      setId(text);
   try{
        AsyncStorage.getItem('user').then(data=>{
           setUser(JSON.parse(data));
        }) 
     }
     catch(er){
       console.log(er);
     }
  }
  
  return(
        <ScrollView style={styles.container}>
          {/* <Text onPress={()=> console.log(user.name)}>ss</Text> */}
            {
            dataCart.map((item) => {
              return(
                <View>
         
            <View style={{flexDirection:'row', backgroundColor:'white', padding:10}}>
               <Image
                 source={image}
                 style={{
                   height:80,
                   width:90
                 }}
               />
               <View style={{flexDirection:'column', paddingLeft: 15,  marginLeft:10}}>
                  <Text style={{fontSize:18}}>Барааны нэр: {item.pname} </Text>
                  <Text style={{fontSize:18, paddingRight: 20}}>Тоо ширхэг: {item.quant}</Text>
               </View>
            </View>
            <View style={{marginTop:2, marginBottom:5, padding:10, backgroundColor:'white', flexDirection:'row'}}>
              <TouchableOpacity style={{flexDirection:'row'}} onPress={()=> remove()}>
                <Icon
                  name='delete-outline'
                  size={26}
                  style={{
                  color:'red'
                  }}

                  />
                <Text style={{fontSize:18, paddingLeft:8, color:'red'}}>Устгах</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{flexDirection:'row', paddingLeft:30}} onPress={()=>saveItem(item)} >
                <Icon
                  name='bookmark-outline'
                  size={26}
                  />
                  <Text style={{fontSize:18, paddingLeft:8}} >Хадгалах</Text>
              </TouchableOpacity>
              <Text style={{fontSize:18, paddingLeft:65}}>₮{item.price}</Text>
             </View>
           </View>
          
            )
          })
          }
          
           <View style={{flexDirection:'row', width:'100%', padding:10, marginBottom:10,marginTop:10, backgroundColor:'white'}}>
            <Text style={{fontSize:18,paddingRight:10}} onPress={()=> makeid()} >Нийт төлөх дүн: ₮ {totalprice}</Text>
           </View>
           <FormButton
             buttonTitle='Захиалах'
             onPress={()=> order()}
           />
           {/* <FormButton
             buttonTitle='Захиалах'
             onPress={()=> savedata()}
           /> */}
        </ScrollView>
  );
}
export default Store;
  const styles = StyleSheet.create({
    container:{
      flex: 1,
      //justifyContent: 'flex-start',
      backgroundColor:'#DEDCDC',
      padding:10
    },
  })