import { Navigation } from '@material-ui/icons';
import React, {Component} from 'react';

import {
    StyleSheet,
    View,
    Text,
    Alert,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


const Profile = ({navigation})=>{
    return(
        <View style={styles.container}>
            <View style={{backgroundColor:'white', padding:10, margin:5, alignItems:'center'}}>
                 <Text style={{fontSize: 16, color:"#000", paddingBottom:10}}>Сайна байна уу,</Text>
                 <Text style={{fontWeight:'bold', fontSize: 18}}>Elaman</Text>
            </View >
            <View style={{backgroundColor:'white', padding:10, margin:5, flexDirection:'row'}}>
                <Icon name="shopping-cart"  size={24}
                   
                 />
                 <Text style={{fontSize:18, paddingLeft:10, width:'88%'}} onPress={()=> navigation.navigate('OrderScreen')}> Миний захиалга</Text>
                 <Icon name="chevron-right" size={20} alignItems="flex-end"/>
            </View>
            <View style={{backgroundColor:'white', padding:10, margin:5, flexDirection:'row'}}>
                <Icon name="bookmark" color="black" size={24}
                 />
                 <Text style={{fontSize:18, paddingLeft:10, width:'88%'}} onPress={()=> navigation.navigate('Bookmark')}>Хадгалсан бараа</Text>
                 <Icon name="chevron-right" size={20} alignItems="flex-end"/>
            </View>
            
        </View>
    )    
}
export default Profile;

const styles = StyleSheet.create({
    container:{
        flex:1,

    }
})