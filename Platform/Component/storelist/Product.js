import { Height } from '@material-ui/icons';
import { StylesProvider } from '@material-ui/styles';
import React, {Component, useEffect, useState} from 'react';
import { TextInput } from 'react-native';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    Button,
    Alert,
    ScrollView,
} from 'react-native';
import FormButton from '../components/FormButton';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import database from '@react-native-firebase/database';
import { parse } from '@babel/core';
import { getIsDrawerOpenFromState } from '@react-navigation/drawer';
const image  = require('../assets/airpod.png')
const Product = ({navigation ,route}) => {
    const p_name = route.params.pname;
    const p_price = route.params.price;
    const [quantity, setQuan] = useState(0);
    const [temp, setTemp] = useState([]);
    
    let totalprice = quantity * p_price;
    
    const addToCart = async() => {
        const cartinfo = {
            pname:  p_name,
            price: totalprice,
            quant: quantity,
         }
        try{
            AsyncStorage.getItem('cart').then((datacart)=>{
                if(datacart !== null){
                    const cart = JSON.parse(datacart);
                    cart.push(cartinfo)
                    AsyncStorage.setItem('cart', JSON.stringify(cart))
                    alert('Сагсанд нэмэгдлээ!');
                }
                else{
                    let cart = []
                    cart.push(cartinfo)
                    AsyncStorage.setItem('cart', JSON.stringify(cart))
                }
            })
        }
        catch(er){
            console.log(er);
        }
    }
    const display = async() =>{
        try{
            AsyncStorage.getItem('cart').then(data =>{
                console.log(data);
            })
            
        }
        catch(er){
            console.log(er);
        }
    }
   
    return(
        <View style={styles.container}>
            <View style={{alignItems:'center',padding:10}}>
            <Image
              source={image}
              style={{
                  height:200,
                  width:240,
              }
              }
            />
            </View>
            <View style={{margin:10, padding:10, paddingLeft:25, backgroundColor:'#FCFAFA'}}>
                <Text style={{fontSize:18, paddingBottom:10}} onPress={()=> getData()}>
                    Барааны нэр: {p_name} 
                </Text>
               <Text style={{fontSize:18,paddingBottom:10 }} >
                    Ширхэгийн үнэ: {p_price}
               </Text>
               <View style={{flexDirection:'row', width:'100%'}}>
               </View>
              <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                 <Text style={{fontSize:18}}>Нийт үнэ: {totalprice}</Text>  
                <View style={{flexDirection:'row',}}>
                        <Icon 
                        name='ios-remove-circle'
                        size={26}
                        style={{color:'#D0C402'}}
                        onPress={()=>setQuan(quantity - 1)}
                        />
                        <Text style={{fontSize:18,fontWeight:'bold', paddingLeft:5, paddingRight:5,}}>
                            {quantity}
                        </Text>
                        <Icon 
                        name='ios-add-circle'
                        size={26}
                        style={{color:'#D0C402'}}
                        onPress={()=> setQuan(quantity + 1)}
                        />
                </View>
               </View>
               <Text style={{fontSize:18, paddingTop: 10}}>
                    Барааны дэлгэрэнгүй
               </Text>
               <Text>Apple компаний бүтээгдэхүүн.</Text>
            </View>
            <FormButton
                  buttonTitle="Сагсанд нэмэх"
                  style={{margin:10, padding:10}}
                  onPress={()=> addToCart()}
               />
               {/* <FormButton
                  buttonTitle="Сагсанд нэмэх"
                  style={{margin:10, padding:10}}
                  onPress={()=> display()}
               /> */}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'#F0F0F0'
    }

})

export default Product;