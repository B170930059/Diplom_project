import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import  {Avatar} from 'react-native-paper';
const image = require('../assets/man.png');
const value = 'Чихэвч';
const OrderDetailsScreen = ({navigation}) => {
    return(
        <View style={styles.container}>
            <Text style={{fontSize: 18}}>
                Барааны нэр: {value}
            </Text>
            <Text style={{fontSize: 18}}>
                Тоо ширхэг:
            </Text>
            <Text style={{fontSize: 18}}>
                Ширхэгийн үнэ:
            </Text>
            <Text style={{fontSize: 18, paddingBottom:10}}>
               Нийт үнэ:
            </Text>
            <Avatar.Image 
                source={image}
                size={50}
                
            />
        </View>
    )
}

export default OrderDetailsScreen;

const styles = StyleSheet.create({
    container: {
      padding: 17,
      flex: 1, 
      alignItems: 'flex-start', 
      justifyContent: 'flex-start'
    }
});