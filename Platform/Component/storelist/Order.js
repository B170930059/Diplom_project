import React, {useState, useEffect} from 'react';

import {
    StyleSheet,
    View,
    Text,
    Alert,
    TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import  {Avatar} from 'react-native-paper';
import { Navigation } from '@material-ui/icons';
import database from '@react-native-firebase/database';
const image = require('../assets/airpod.png');
const imagu = require('../assets/promax.png');

const Order = ({navigation}) =>{
 const [ordercode, setOrderCode] = useState([]);
 
 useEffect(()=>{
    try{
      const ref = database().ref('/order');
      const listener = ref.on('value', (snapshot)=> {
      
        let temp = [];
        snapshot.forEach((doc) => {
          let data = doc.val();
          temp.push(data);
        }); 
        setOrderCode(temp);
      });
      return () =>{
        ref.off('value', listener);
      }
    }
    catch(err){
      console.log(err);
    }
 });
    return(
        <View style={styles.container}>
            {
                ordercode.map((item, i) =>{
                    return(
                   <View style={{paddingLeft: 10, paddingTop:10, paddingRight:10}}>     
                   <TouchableOpacity style={{backgroundColor:'white', padding:8, paddingTop:1}} >
                    <View style={{flexDirection:'row', paddingTop: 10}}>
                        <Avatar.Image 
                            source={image}
                            size={50}
                        />
                        <View style={{flexDirection:'column'}}>
                            <Text style={styles.text} >
                                {item.id}
                            </Text>
                            <Text style={styles.text}>
                                {item.date}
                            </Text>     
                        </View>    
                    </View>
                    </TouchableOpacity>    
                    </View>
                    )
                })
            }
            {/* <View style={{flexDirection:'row', margin:8, padding: 10, backgroundColor:'white'}}>
            <Avatar.Image 
                source={image}

                size={50}
            />
            <View style={{flexDirection:'column'}}>
                <Text style={styles.text} onPress={()=> navigation.navigate('OrderScreen')}>
                    #FGJAUS
                </Text>
                <Text style={styles.text}>
                    2021.05.02
                </Text>     
            </View>    
          </View>
          <View style={{flexDirection:'row', margin:8, padding: 10, backgroundColor:'white'}}>
            <Avatar.Image 
                source={imagu}

                size={50}
            />
            <View style={{flexDirection:'column'}}>
                <Text style={styles.text}>
                    #UGHAUS
                </Text>
                <Text style={styles.text}>
                    2021.05.1
                </Text>     
            </View>    
          </View> */}
        </View>
    )
}

export default Order;

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:10
    },
    text:{
        fontSize: 18,
        paddingLeft: 13,
        flexDirection:'column',
        color: '#493F50',
      }
})