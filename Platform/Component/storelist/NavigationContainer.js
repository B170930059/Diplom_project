import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import {  NavigationContainer} from '@react-navigation/native';
import MainTabStore from '../store/MainTabStore';

import { DrawerContent } from '../userscreens/DrawerContent';
import Storelist from './Store'; 
import MainTabScreen from '../userscreens/MainTabScreen';
import RootStack from '../userscreens/RootStackScreen';
const Drawer = createDrawerNavigator();

const NavigationContain = (props) => {
  return( 
    <NavigationContainer>
       <Drawer.Navigator drawerContent = {props => <DrawerContent {...props}/>}>
          {/* <Drawer.Screen name="MainTab" component={MainTabScreen}/> */}
          
          <Drawer.Screen name="RootStack" component={RootStack}/>
          <Drawer.Screen name="MainStore" component={MainTabStore}/>
       </Drawer.Navigator>
    </NavigationContainer>
  );
} 

export default NavigationContain;