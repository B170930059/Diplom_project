import React, {Component} from 'react';

import {
    StyleSheet,
    View,
    Text,
    Alert,
    TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import  {Avatar} from 'react-native-paper';
import database from '@react-native-firebase/database';
const image = require('../assets/airpod.png');
const imagu = require('../assets/promax.png');

const Bookmark = () =>{

    return(
        <View style={styles.container}>
            <View style={{flexDirection:'row', margin:8, padding: 10, backgroundColor:'white'}}>
            <Avatar.Image 
                source={image}
                size={50}
            />
            <View style={{flexDirection:'column', width:'78%'}}>
                <Text style={styles.text}>
                    Airpod pro 6
                </Text>
                <Text style={styles.text}>
                    ₮90000
                </Text>     
            </View>   
            <Icon name="cancel" size={30} color='#ED0000'/> 
          </View>
          <View style={{flexDirection:'row',margin:8, padding: 10, backgroundColor:'white'}}>
            <Avatar.Image 
                source={imagu}
                size={50}
            />
            <View style={{flexDirection:'column', width:'78%'}}>
                <Text style={styles.text}>
                    Iphone 12 pro max
                </Text>
                <Text style={styles.text}>
                    ₮4500000
                </Text>     
            </View>    
            <Icon name="cancel" size={30} color='#ED0000'/> 
          </View>
        </View>
    )
}

export default Bookmark;

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:10
    },
    text:{
        fontSize: 18,
        paddingLeft: 13,
        flexDirection:'column',
        color: '#493F50',
      }
})