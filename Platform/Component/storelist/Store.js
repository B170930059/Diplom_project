import React, { useEffect, useState } from 'react'

import {
  StyleSheet,
  View,
  TextInput,
  Text,
  Image,
  ScrollView,
  Dimensions,
} from 'react-native'
import { SearchBar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5'
import {createStackNavigator} from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { BorderBottom, Home } from '@material-ui/icons';
import { Tab } from '@material-ui/core';
import ProfileScreen from './ProfileScreen';
import { ImageStore } from 'react-native';
import { TouchableOpacity } from 'react-native';
import database from '@react-native-firebase/database';
import { FlatList } from 'react-native-gesture-handler';
const {width} = Dimensions.get("window");
const height = width *0.6;
const images=[
   'https://dlb99j1rm9bvr.cloudfront.net/airpods-pro/parts/angle-1/model/size-1000/bg.png',
   'https://www.zdnet.com/a/hub/i/r/2020/11/22/bf9b5a66-d316-41c9-826a-993928becb51/resize/1200xauto/513715a4d17276938a21b69f46c34811/apple-iphone-12-lineup.jpg',
   'https://pcloft.com/wp-content/uploads/2020/12/Best-Laptop-for-Gaming-And-Everyday-Use.jpg'
]
const imagu = require('../assets/promax.png');
const imago = require('../assets/airpod.png');

const HomeStack  = createStackNavigator();
const ProfileStack = createStackNavigator();
const MainTab = createMaterialBottomTabNavigator();

const Store = ({navigation})=>{
  const [image, setImage] = useState();
  const [active, setState] = useState();
  const [product, setProduct] = useState([]);
  const [pname, setPname] = useState([]);
  const price = 2500000;
  const change = ({nativeEvent}) => {
    const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
     if(slide !== active){
       setState({active : slide});
     }
  }

  useEffect(() => {
    const ref = database().ref('/product');
    const listener = ref.on('value', snapshot => {
      let temp = [];
      snapshot.forEach((doc) => {
        let data = doc.val();
        temp.push(data);
       }); 
      setProduct(temp);
      // product.forEach(el =>{
      //   console.log(el[0]);
      // })
     });
    return () =>{
      ref.off('value', listener);
    }
  });
  const show = () => {
    for(let i = 0; i < product.length; i++){
        pname.push(product[i]);
    }
    console.log(pname);
  }
  return(
        <View style={styles.container}>
          <View style={{padding:5}}>
          <TextInput
            placeholder='Хайх утга оруул'
            backgroundColor='white'
            style={{
              borderRadius:10,
              borderTopColor:'white',
              paddingLeft:10,
                }}
          />
          </View>
           <ScrollView 
            pagingEnabled
            horizontal
            onScroll={change}
            showsHorizontalScrollIndicator={false}
            style={{width, height}}>
              {
                images.map((image, index) => (
                  <Image
                   key={index}
                   source={{uri:image}}
                   style={{width, height, resizeMode:'cover'}}/>
                ))
              }
           </ScrollView>
           <View style={{flexDirection:'row', bottom: 0, alignSelf:'center'}}>
              {
                images.map((i, k)=>(
                  <Text key={k} style={k == active ? styles.pagingActiveText  : styles.pagingText}>⬤</Text>
                ))
              }
           </View>
           <View style={{ paddingTop:10, backgroundColor:'#E4E4E4'}}>
             <Text style={{fontSize:20, paddingLeft:13}} onPress={()=> show()}>
                Технологи
             </Text>
             <ScrollView
             pagingEnabled
             vertical
             > 
             {
               product.map((data, index) => 
               {
                for(let i = 0; i < product.length; i++){
                  return(
                    <View style={{flexDirection:'row', padding:15}}>
                    <TouchableOpacity style={{width:'50%'}} key={index} onPress={()=> navigation.navigate('Product', {
                      pname: product[i].productname,
                      price: product[i].price
                    })}> 
                     <View style={{ margin:10, flexDirection:'column'}}>
                         <Image
                         source={imago}
                         style={{
                           resizeMode: "cover",
                           height: 150,
                           width: '100%',
                           borderRadius: 15,
                           borderTopLeftRadius:0
                         }}
                         />
                         <View style={{
                           //position: 'absolute',
                           bottom:0,
                           height: 35,
                           width: '100%',
                           backgroundColor: 'white',
                           borderTopRightRadius: 15,
                           borderBottomLeftRadius: 15,
                           alignContent:'center',
                           justifyContent:'center',
                         }}>
                           <Text style={{fontSize:14, paddingLeft:10}}>{product[i].productname}</Text> 
                           <Text style={{fontSize:14, paddingLeft:10}}>Үнэ: {product[i].price}₮</Text>            
                         </View>
                         
                       </View>
                     </TouchableOpacity> 
                     <TouchableOpacity style={{width:'50%'}} onPress={()=> navigation.navigate('Product', {
                        pname: product[i+1].productname,
                        price: product[i+1].price
                     })}> 
                     <View style={{margin:10}}>
                         <Image
                         source={imagu}
                         style={{
                           resizeMode: "cover",
                           height: 150,
                           width: '100%',
                           borderRadius: 15,
                           borderTopLeftRadius:0
                         }}
                         />
                          <View style={{
                           //position: 'absolute',
                           bottom:0,
                           height: 35,
                           width: '100%',
                           backgroundColor: 'white',
                           borderTopRightRadius: 15,
                           borderBottomLeftRadius: 15,
                           alignContent:'center',
                           justifyContent:'center',
                         }}>
                           <Text style={{fontSize:14, paddingLeft:10}}>{product[i+1].productname}</Text> 
                           <Text style={{fontSize:14, paddingLeft:10}}>Үнэ: {product[i+1].price}₮</Text>            
                         </View>
                     </View>
                    </TouchableOpacity> 
                    
                  </View>
                  
                  )
                 }
               })
             }
            </ScrollView>
           </View>
           
       </View>
        
  );
}
export default Store;
  const styles = StyleSheet.create({
    container:{
      justifyContent: 'flex-start',
      backgroundColor:'#E5E5E0',
      //backgroundColor:'skyblue',
      
    },
    pagingText:{ 
      color:'#888', 
      margin: 3
    },

    pagingActiveText: {
      color:'#fff',
      margin:3}
  })