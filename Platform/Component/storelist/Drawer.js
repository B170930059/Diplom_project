import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import {  NavigationContainer} from '@react-navigation/native';
import MainTabStore from '../store/MainTabStore';

import { DrawerContent } from '../userscreens/DrawerContent';
import Storelist from './Store'; 
import MainTabScreen from '../userscreens/MainTabScreen';
import RootStack from '../userscreens/RootStackScreen';
const Drawer = createDrawerNavigator();

const DrawerScreen = (props) => {
 
  return( 
    <NavigationContainer>
       <Drawer.Navigator drawerContent = {props => <DrawerContent {...props}/>}>
          <Drawer.Screen name="MainScreen" component={MainTabScreen}/>
       </Drawer.Navigator>
    </NavigationContainer>
    
  );
} 

export default DrawerScreen;